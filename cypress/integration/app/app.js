import { Given, Then } from "cypress-cucumber-preprocessor/steps";

const base_url = "http://localhost:4200/";

// ***************************
// App Booting
// ***************************

Given("I open up the page", () => {
  cy.visit(base_url);
});

Then("I should see a button", () => {
  const button = cy.get("button");

  button.should(res => {
    expect(res).to.have.length(1);
  });
});
