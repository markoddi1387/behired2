export const environment = {
  production: false,
  // baseUrl: 'https://behiredserver.co.uk/',
  baseUrl:  'https://behired-be.ew.r.appspot.com/',
  googleApiKey: 'AIzaSyA85bGgk2hSGHT494X7ozlefPywx0NbtNc',
  googleApiMap: 'https://maps.googleapis.com/maps/api/geocode/json',
  rssKey: 'eh7avjomanr1gapxep8rax0cgcecn1ypycxv65tc',
  firebase: {
    cdn: 'gs://behired-staging.appspot.com',
    apiKey: 'AIzaSyCF3hMyDe4xFr5qNmHGMzWsH9BkQdgTCPI',
    authDomain: 'behired-staging.firebaseapp.com',
    databaseURL: 'https://behired-staging.firebaseio.com',
    projectId: 'behired-staging',
    storageBucket: 'behired-staging.appspot.com',
    messagingSenderId: '1006235072572'
  }
};
