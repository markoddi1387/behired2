import { MetaGuard } from '@ngx-meta/core';

import { Routes } from '@angular/router';

// Layouts:
import { SidebarLayout } from '../../layouts/sidebar/sidebar.layout';

// Components:
import { ApplicantJobsComponent } from './applicant-jobs/applicant-jobs.component';
import { ApplicantProfileComponent } from './applicant-profile/applicant-profile.component';

// Guards:
import { AuthGuard } from '../../guards/auth.guard';

const TITLE = 'Behired |';

export const ROUTES: Routes = [
  {
    path: '',
    canActivateChild: [ MetaGuard ],
    component: SidebarLayout,
    canActivate: [ AuthGuard ],
    children: [
      {
        path: 'profile',
        component: ApplicantProfileComponent,
        data: { title: `${ TITLE } Profile`,
          meta: {
            title: `${ TITLE } Profile`,
            description: 'Behired profile'
          }
        }
      },
      {
        path: 'jobs',
        component: ApplicantJobsComponent,
        data: { title: `${ TITLE } Jobs`,
          meta: {
            title: `${ TITLE } Jobs`,
            description: 'Behired jobs'
          }
        }
      },
    ]
  }
];
