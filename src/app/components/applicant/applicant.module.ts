import { CommonModule } from '@angular/common';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { NgModule } from '@angular/core';
import { NgxErrorsModule } from '@hackages/ngxerrors';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PreventDoubleSubmitModule } from 'ngx-prevent-double-submission';
import { TruncateModule } from 'ng2-truncate';

import { ROUTES } from './applicant.routes';

// Modules
import { SharedModule } from '../../shared/shared.module';
import { LayoutModule } from '../../layouts/layout.module';

// Components:
import { ApplicantJobsComponent } from './applicant-jobs/applicant-jobs.component';
import { ApplicantProfileComponent } from './applicant-profile/applicant-profile.component';

@NgModule({
  imports: [
    GooglePlaceModule,
    PreventDoubleSubmitModule.forRoot(),
    CommonModule,
    FormsModule,
    TruncateModule,
    NgxErrorsModule,
    LayoutModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(ROUTES)
  ],
  declarations: [
    ApplicantJobsComponent,
    ApplicantProfileComponent
  ]
})
export class ApplicantModule { }
