import { Component, Inject, OnInit, OnDestroy, PLATFORM_ID } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject, Subscription } from 'rxjs';
import { take, takeUntil, flatMap } from 'rxjs/operators';
import { isPlatformBrowser } from '@angular/common';

import { ApplicantModel } from '../../../models/applicant.model';
import { UserService } from '../../../services/user.service';
import { FileService } from '../../../services/file.service';
import { StorageService } from '../../../services/storage.service';
import { GoogleApiService } from '../../../services/google-api.service';

@Component({
  selector: 'applicant-profile-view',
  templateUrl: './applicant-profile.component.html',
  styleUrls: ['./applicant-profile.component.scss']
})
export class ApplicantProfileComponent implements OnInit, OnDestroy {
  public cv: string;
  public cvHover = false;
  public fileTypesAccepted = [ 'application/doc', 'application/pdf' ];
  public form: FormGroup;
  public isUpdated = false;
  public logo: string| null = null;
  public logoHover = false;
  public uid: string;
  public uploadingCV = false;
  public isViewReady = false;
  public uploadingLogo = false;
  public destroyed$: Subject<boolean> = new Subject<boolean>();
  public options = { types: [], componentRestrictions: { country: 'GB' } };
  public profile: boolean | object = false;
  public isLogoUploadError = false;
  public isCvUploadError = false;
  public cvName: string;
  public statusChanges: Subscription;
  public isErrorsVisible = false;
  public isGoogleReady = false;

  constructor(
    private fileService: FileService,
    private formBuilder: FormBuilder,
    private googleApiService: GoogleApiService,
    private storageService: StorageService,
    private userService: UserService,
    @Inject(PLATFORM_ID) private platform: object
  ) {}

  ngOnInit(): void {
    this.createForm();
    this.getProfile();
    this.formListener();
    this.enableGoogleAutoComplete();
  }

  ngOnDestroy(): void {
    this.destroyed$.next(false);
    this.destroyed$.complete();
  }

  public enableGoogleAutoComplete(): void {
    if (isPlatformBrowser(this.platform)) {
      this.isGoogleReady = true;
    }
  }

  public formListener(): void {
    this.statusChanges = this.form.statusChanges.subscribe(() => this.isErrorsVisible = false);
  }

  public uploadFile(event: FileList, folder: string, name: string): void {
    const file = event.item ? event.item(0) : event['target'].files[0];

    if (this.validateFileUpload(file, name)) {
      const url = `${ folder }/${ this.uid }/${ name }`;
      const uploading = this.fileService.uploadFile(url, file);

      if (name === 'logo') {
        this.uploadingLogo = true;
      }

      if (name === 'cv') {
        this.uploadingCV = true;
        this.form.patchValue({
          cvName: file.name,
        });
      }

      uploading.snapshotChanges()
        .pipe(
          takeUntil(this.destroyed$)
        ).subscribe((data) => {
          if (data.bytesTransferred === data.totalBytes) {
            this.getFile(url, name, true);
            this.submit(true);
          }
        });
    }
  }

  public validateFileUpload(file: any, type: string): boolean {
    if (type === 'logo') {
      if (!(file.type === 'image/png' || file.type === 'image/jpg' || file.type === 'image/jpeg') && file.size <= 8000000) {
        this.isLogoUploadError = true;
        setTimeout(() => this.isLogoUploadError = false, 6000);
        return false;
      }
    } else {
      if (!(file.type === 'application/pdf' || file.type === 'application/msword' || file.type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') && file.size <= 8000000) {
        this.isCvUploadError = true;
        setTimeout(() => this.isCvUploadError = false, 6000);
        return false;
      }
    }

    return true;
  }

  public hoverLogo(isHovering: boolean) {
    this.logoHover = isHovering;
  }

  public hoverCv(isHovering: boolean) {
    this.cvHover = isHovering;
  }

  public getProfile(): void {
    this.uid = this.storageService.getUid();
    this.userService.getApplicantProfile()
      .pipe(
        take(1),
      ).subscribe((profile: ApplicantModel) => {
        if (profile) {
          this.profile = (profile.address && profile.address.latitude) ? profile : false;
          this.form.patchValue(profile);

          if (profile.cv) {
            this.getFile(`cvs/${ this.uid }/cv`, 'cv');
          }

          if (profile.logo) {
            this.getFile(`logos/${ this.uid }/logo`, 'logo');
          }
        }
        setTimeout(() => this.isViewReady = true, 500);
      });
  }

  public createForm(): void {
    const phoneRegex = /^\d{4}[-\s]?\d{3}[-\s]?\d{4}$/;

    this.form = this.formBuilder.group({
      applicantId: ['', [ ]],
      email: ['', [ ]],
      firstName: [ , [ Validators.required, Validators.minLength(1), Validators.maxLength(60) ]],
      lastName: [ , [ Validators.required, Validators.minLength(1), Validators.maxLength(60) ]],
      address: this.formBuilder.group({
        addressLine1: ['', [ ]],
        placeId: [null, [ Validators.required ]],
        longitude: [ '', []],
        latitude: [ '', []]
      }),
      about: [ , [ ]],
      cover: [ , [ ]],
      cv: [ , [ ]],
      cvName: [ , [ ]],
      logo: [ , [ ]],
      mobileNumber: [ , [ Validators.pattern(phoneRegex) ]]
    });
  }

  public handleAddressChange(address: any): void {
    this.form.patchValue({
      address: {
        addressLine1: address.formatted_address,
        placeId: address.place_id
      }
    });
  }

  public submit(isFileUpload?: boolean): void {
    if (this.form.valid) {
      const form = { ...this.form.value };

      this.googleApiService.getAddress(form.address.placeId)
        .pipe (
          take(1),
          flatMap((location) => {
            this.form.patchValue({
              address: {
                addressLine1: location['address'],
                latitude: location['lat'],
                longitude: location['lng']
              }
            });

            return this.userService.updateApplicantProfile(this.form.value);
          })
        ).subscribe(() => {
          this.isUpdated = true;
          setTimeout(() => this.isUpdated = false, 3000);
          this.getProfile();
        });
    } else {
      this.isErrorsVisible = true;
      window.scroll(0, 0);
    }

    if (isFileUpload) {
      this.userService.updateApplicantProfile(this.form.value);
    }
  }

  public getFile(url: string, type: string, isUpdate = false): void {
    this.fileService.getFile(url, type)
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe(link => {
        this[type] = link;

        if (type === 'logo') {
          this.form.patchValue({
            logo: link
          });
        }

        if (type === 'cv') {
          this.form.patchValue({
            cv: link
          });
        }

        this.uploadingLogo = false;
        this.uploadingCV = false;

        if (isUpdate) {
          this.submit(true);
        }
      }, error => setTimeout(() => this.getFile(url, type, isUpdate), 1000));
  }

  public deleteFile(folder: string, name: string): void {
    const url = `${ folder }/${ this.uid }/${ name }`;
    this.fileService.deleteFile(url)
      .pipe(
        take(1)
      ).subscribe(() => {
        this[name] = null;
        this.form.value[name] = null;

        if (name === 'logo') {
          this.form.patchValue({ logo: null });
        } else {
          this.form.patchValue({ cv: null, cvName: null });
        }

        this.submit(true);
      });
  }
}
