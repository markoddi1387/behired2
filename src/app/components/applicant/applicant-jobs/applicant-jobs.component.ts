import { JobModel } from './../../../models/job.model';
import { Component, OnInit } from '@angular/core';
import { take, map } from 'rxjs/operators';

import { JobService } from '../../../services/job.service';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'applicant-jobs-view',
  templateUrl: './applicant-jobs.component.html',
  styleUrls: ['./applicant-jobs.component.scss']
})
export class ApplicantJobsComponent implements OnInit {
  public jobs: Array<any> = null;
  public isViewReady = false;

  constructor(
    private jobService: JobService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.getJobsAppliedFor();
  }

  public getJobsAppliedFor(): void {
    this.userService.getApplicantProfile()
      .pipe(
        take(1),
        map(jobs => jobs.listOfJobApplications)
      ).subscribe(jobs => {
        this.jobs = jobs;
        this.getJobsStatus(jobs);
      });
  }

  public getJobsStatus(jobs: any): void {
    if (jobs) {
      jobs.forEach(job => {
        this.jobService.getJobById(job.key)
          .pipe(
            take(1)
          ).subscribe(response => {
            job['isActive'] = (response !== null) ? response.isActive : false;
          });
      });
    }

    this.isViewReady = true;
  }
}
