
import { Component, OnInit} from '@angular/core';
import { take } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ReviewService } from '../../../services/review.service';

@Component({
  selector: 'review-form-view',
  templateUrl: './review-form.component.html',
  styleUrls: [ './review-form.component.scss' ]
})
export class ReviewFormComponent implements OnInit {

  public form: FormGroup;

  constructor(
    private reviewService: ReviewService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.createForm();
  }

  public createForm(): void {
    this.form = this.formBuilder.group({
      title: [ 'Turtle Tots', [ Validators.required ] ],
      logo: [ 'https://firebasestorage.googleapis.com/v0/b/leisure-project.appspot.com/o/reviews%2Fturtletots.png?alt=media&token=842cb622-36b9-4fef-a673-c3abe63c7acc', [ ] ],
      reviews: [ [], [] ],
      stats: [ {
        overallScore: 0,
        recommendToAFriend: 0,
        service: 0,
        quality: 0,
        cleanliness: 0,
        communication: 0,
        safety: 0,
        workExperience: 0,
        userCounter: 0
      }, [ ] ],
      jobs: [ 0, [ Validators.required ] ],
      photos: [ [], [] ],
      flag: [ false, [ Validators.required ] ],
      map: [ , [] ],
      about: [ ' ', [ Validators.required ] ],
      type: [ 'SwimSchool', [ Validators.required ] ],
      isActive: [ true, [ Validators.required ] ],
      phone: [ ' ', [ Validators.required ] ],
      website: [ ' ', [ Validators.required ] ],
      email: [ ' ', [ Validators.required ] ]
    });
  }

  public postReview(): void {
    if (this.form.valid) {
      this.reviewService.postReview(this.form.value)
        .pipe(
          take(1)
        ).subscribe(() => console.log('Review posted...'));
    }
  }
}
