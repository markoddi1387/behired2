
import { Component, OnInit} from '@angular/core';
import { take } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';

import { ReviewService } from '../../../services/review.service';
import { ReviewModel } from '../../../models/review.model';
import { CommentModel } from '../../../models/comment.model';
import { AuthService } from '../../../services/auth.service';
import { JobModel } from '../../../models/job.model';
import { JobService } from './../../../services/job.service';
import { BlogModel } from '../../../models/blog.model';
import { BlogService } from '../../../services/blog.service';

const STARS = 5;
const STAR_VALUE = 20;
const QUESTIONS = 7;

@Component({
  selector: 'review-view',
  templateUrl: './review.component.html',
  styleUrls: [ './review.component.scss' ]
})
export class ReviewComponent implements OnInit {

  public isScoreModalVisible = false;
  public isReviewModalVisible = false;
  public isLoginModalVisible = false;
  public currentReviewId: null | string = null;
  public listOfJobs = [];
  public listOfBlogs = [];
  public review: ReviewModel;
  public numberOfVotes = 0;
  public totalScore = 0;
  public totalQualityScore = 0;
  public totalCommunicationScore = 0;
  public totalServiceScore = 0;
  public totalRecommendToAFriend = 0;
  public totalCleanliness = 0;
  public totalSafety = 0;
  public totalWorkExperience = 0;

  constructor(
    private authService: AuthService,
    private reviewService: ReviewService,
    private activatedRoute: ActivatedRoute,
    private blogService: BlogService,
    private jobService: JobService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.listener();
    this.authCheck();
  }

  public authCheck(): void {
    this.authService.getUser()
      .pipe(
        take(1)
      ).subscribe((user) => {
        this.isLoginModalVisible = (user === null);
        this.getLatestJobs();
        this.getLatestBlogs();
      });
  }

  public listener(): void {
    this.activatedRoute.params.pipe(
      take(1)
    ).subscribe(params => {
      if (params['id']) {
        this.currentReviewId = params['id'];

        if (params['type']) {
          if (params['type'] === 'rate') {
            this.isScoreModalVisible = true;
          } else {
            this.isReviewModalVisible = true;
          }
        }

        this.getReview();
      }
    });
  }

  public getReview(): void {
    this.reviewService.getReviewsById(this.currentReviewId)
    .pipe(
      take(1)
    ).subscribe((review: ReviewModel) => {
      this.review = review;
      this.setStats();
      this.calcOveralScore();
    });
  }

  public setStats(): void {
    const { quality, recommendToAFriend, safety, workExperience, cleanliness,
      communication, service, userCounter } = this.review.stats;

    this.totalQualityScore = this.calcTotalPercentage(quality, userCounter);
    this.totalCommunicationScore = this.calcTotalPercentage(communication, userCounter);
    this.totalServiceScore = this.calcTotalPercentage(service, userCounter);
    this.totalRecommendToAFriend = this.calcTotalPercentage(recommendToAFriend, userCounter);
    this.totalCleanliness = this.calcTotalPercentage(cleanliness, userCounter);
    this.totalSafety = this.calcTotalPercentage(safety, userCounter);
    this.totalWorkExperience = this.calcTotalPercentage(workExperience, userCounter);

    this.numberOfVotes = userCounter;
  }

  public calcTotalPercentage(value: number, userCounter: number): number {
    if (userCounter !== 0) {
      const maxTotal = STARS * userCounter;
      const total = (value / maxTotal) * 100;
      return Math.round(total);
    }
    return 0;
  }

  public calcOveralScore(): void {
    const { quality, recommendToAFriend, safety, workExperience, cleanliness,
      communication, service, userCounter } = this.review.stats;

    if (userCounter !== 0) {
      const totalScores = quality + communication + service + recommendToAFriend + safety + cleanliness + workExperience;
      const maxTotal = (STARS * QUESTIONS) * userCounter;
      const total = ((totalScores / maxTotal) * 100) / STAR_VALUE;
      this.totalScore = Math.round(total);
    } else {
      this.totalScore = 5;
    }
  }

  public updateReviewById(event: any): void {
    this.isScoreModalVisible = false;
    this.isReviewModalVisible = false;

    if (event.review) {
      this.review.reviews = [ ...this.review.reviews, event as CommentModel ];
    } else {
      this.review.stats.communication += event.communication;
      this.review.stats.service += event.service;
      this.review.stats.quality += event.quality;
      this.review.stats.safety += event.safety;
      this.review.stats.recommendToAFriend += event.recommendToAFriend;
      this.review.stats.cleanliness += event.cleanliness;
      this.review.stats.workExperience += event.workExperience;
      this.review.stats.userCounter++;
    }

    this.reviewService.updateReviewById(this.currentReviewId, this.review)
      .pipe(
        take(1)
      ).subscribe(() => {
        this.setStats();
        this.calcOveralScore();
      });
  }

  public getLatestJobs(): void {
    this.jobService.getAllActiveBehiredJobsByLimit()
      .pipe(
        take(1)
      ).subscribe((jobs: Array<JobModel>) => this.listOfJobs = jobs);
  }

  public getLatestBlogs(): void {
    this.blogService.getAllBlogs('@behired')
      .pipe(
        take(1)
      ).subscribe((blogs: Array<BlogModel>) => this.listOfBlogs = blogs);
  }

  public selectedBlog(title: string): void {
    const slug = title.replace(/[&\/\\#,+()$~%.'":——*?!<>{}]/g, '').replace(/\s+/g, '-').toLowerCase();
    this.router.navigate([`/blog/${ slug }`]);
  }
}
