import { MetaGuard } from '@ngx-meta/core';
import { Routes } from '@angular/router';

// Layouts:
import { FullWidthLayout } from '../../layouts/full-width/full-width.layout';

// Components:
import { ReviewsComponent } from './reviews/reviews.component';
import { ReviewFormComponent } from './review-form/review-form.component';
import { ReviewComponent } from './review/review.component';

const TITLE = 'Behired |';

export const ROUTES: Routes = [
  {
    path: '',
    canActivateChild: [ MetaGuard ],
    component: FullWidthLayout,
    children: [
      {
        path: '',
        component: ReviewsComponent,
        data: { title: `${ TITLE } Reviews`,
          meta: {
            title: `${ TITLE } Review`,
            description: 'Behired reviews'
          }
        }
      },
      {
        path: 'company/:id',
        component: ReviewComponent,
        data: { title: `${ TITLE } Review`,
          meta: {
            title: `${ TITLE } Review`,
            description: 'Behired review'
          }
        }
      },
      {
        path: 'company/:id/:type',
        component: ReviewComponent,
        data: { title: `${ TITLE } Review`,
          meta: {
            title: `${ TITLE } Review`,
            description: 'Behired review'
          }
        }
      },
      {
        path: 'review-form',
        component: ReviewFormComponent,
        data: { title: `${ TITLE } Review Form`,
          meta: {
            title: `${ TITLE } Review Form`,
            description: 'Behired review form'
          }
        }
      }
    ]
  }
];
