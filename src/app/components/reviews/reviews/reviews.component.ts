import { Component, OnInit} from '@angular/core';
import { take } from 'rxjs/operators';

import { ReviewService } from '../../../services/review.service';
import { ReviewModel } from '../../../models/review.model';

@Component({
  selector: 'reviews-view',
  templateUrl: './reviews.component.html',
  styleUrls: [ './reviews.component.scss' ]
})
export class ReviewsComponent implements OnInit {

  public reviews: Array<ReviewModel> = [];

  constructor(
    private reviewService: ReviewService
  ) {}

  ngOnInit(): void {
    this.getReviews();
  }

  public getReviews(): void {
    this.reviewService.getReviewsByType('SwimSchool')
      .pipe(
        take(1)
      ).subscribe((reviews: Array<ReviewModel>) => {
        this.reviews = reviews;
      });
  }
}
