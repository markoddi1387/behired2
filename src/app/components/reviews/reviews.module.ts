
import { CommonModule } from '@angular/common';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { NgModule } from '@angular/core';
import { NgxErrorsModule } from '@hackages/ngxerrors';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PreventDoubleSubmitModule } from 'ngx-prevent-double-submission';
import { TruncateModule } from 'ng2-truncate';

import { ROUTES } from './reviews.routes';

// Modules
import { SharedModule } from '../../shared/shared.module';
import { LayoutModule } from '../../layouts/layout.module';

// Components:
import { ReviewsComponent } from './reviews/reviews.component';
import { ReviewFormComponent } from './review-form/review-form.component';
import { ReviewComponent } from './review/review.component';

@NgModule({
  imports: [
    LayoutModule,
    GooglePlaceModule,
    PreventDoubleSubmitModule.forRoot(),
    CommonModule,
    TruncateModule,
    FormsModule,
    NgxErrorsModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(ROUTES)
  ],
  declarations: [
    ReviewsComponent,
    ReviewComponent,
    ReviewFormComponent
  ]
})
export class ReviewsModule { }
