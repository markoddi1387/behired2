import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { take } from 'rxjs/operators';
import { Subscription } from 'rxjs';

import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'forgot-password-view',
  templateUrl: './forgot-password.component.html'
})
export class ForgotPasswordComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public error: boolean | string;
  public isErrorsVisible = false;
  public statusChanges: Subscription;
  public success: boolean | string;

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.createForm();
    this.formListener();
  }

  ngOnDestroy() {
    this.statusChanges.unsubscribe();
  }

  public formListener(): void {
    this.statusChanges = this.form.statusChanges.subscribe(() => this.isErrorsVisible = false);
  }

  public createForm(): void {
    const defaultEmail = this.authService.getCurrentEmail;
    this.form = this.formBuilder.group({
      email: [ defaultEmail, [
        Validators.email,
        Validators.required
      ]]
    });
  }

  public onSubmit(): void {
    this.error = false;
    this.success = false;
    const { email } = this.form.value;

    if (this.form.valid) {
      this.authService.sendForgotPassword(email)
        .pipe(
          take(1)
        ).subscribe(data => {
          if (!data) {
            this.success = 'Please visit your emails to change your password';
          } else {
            this.error = data.message;
          }
        });
    } else {
      this.isErrorsVisible = true;
    }
  }
}
