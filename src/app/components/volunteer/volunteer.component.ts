import { Component } from '@angular/core';

@Component({
  selector: 'volunteer-view',
  templateUrl: './volunteer.component.html',
  styleUrls: ['./volunteer.component.scss']
})
export class VolunteerComponent {
  public steps = [true, false, false];

}
