import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { take } from 'rxjs/operators';
import { Subscription } from 'rxjs';

import { AuthService } from '../../services/auth.service';
import { StorageService } from '../../services/storage.service';
import { RouterService } from '../../services/router.service';

@Component({
  selector: 'welcome-back-view',
  templateUrl: './welcome-back.component.html',
  styleUrls: []
})
export class WelcomeBackComponent implements OnInit, OnDestroy {
  public isViewReady = false;
  public email: string;
  public form: FormGroup;
  public statusChanges: Subscription;
  public isErrorsVisible = false;

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private routerService: RouterService,
    private activatedRoute: ActivatedRoute,
    private storageService: StorageService,
    private zone: NgZone
  ) {}

  ngOnInit(): void {
    this.createForm();
    this.getEmail();
    this.formListener();
  }

  ngOnDestroy() {
    this.statusChanges.unsubscribe();
  }

  public formListener(): void {
    this.statusChanges = this.form.statusChanges.subscribe(() => this.isErrorsVisible = false);
  }

  public createForm(): void {
    const passwordRegex = '(?!^[0-9]*$)(?!^[a-zA-Z]*$)^(.{8,15})$';

    this.form = this.formBuilder.group({
      password: [, [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(15),
        Validators.pattern(passwordRegex)
      ]]
    });
  }

  public getEmail(): void {
    this.activatedRoute.params
      .pipe(
        take(1)
      ).subscribe(params => {
        if (params['email']) {
          this.email = params['email'];
          this.signIn(this.email, 'Beh1rdP@ssw0rd123!');
        }
      });
  }

  public signIn(email: string, password: string): void {
    this.authService.login(email, password)
      .pipe(
        take(1)
      ).subscribe((data) => {
        this.zone.run(() => {
          if (data.user) {
            this.storageService.setUid(data.user.uid);
            this.isViewReady = true;
          }
        });
      });
  }

  public onSubmit(): void {
    const { password } = this.form.value;

    if (this.form.valid) {
      this.authService.changePassword(password)
        .pipe(
          take(1)
        ).subscribe(() => this.routerService.redirectProfile());
    } else {
      this.isErrorsVisible = true;
    }
  }
}
