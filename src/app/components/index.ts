import { AboutComponent } from './about/about.component';
import { FaqComponent } from './faq/faq.component';
import { AccountManagementComponent } from './account-management/account-management.component';
import { ContactComponent } from './contact/contact.component';
import { CookieComponent } from './cookie/cookie.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { HomeComponent } from './home/home.component';
import { JobBoardComponent } from './job-board/job-board.component';
import { JobBoardItemComponent } from './job-board-item/job-board-item.component';
import { JobAdzunaCardComponent } from './job-board/job-adzuna-card/job-adzuna-card.component';
import { JobBehiredCardComponent } from './job-board/job-behired-card/job-behired-card.component';
import { SigninComponent } from './signin/signin.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { SignupComponent } from './signup/signup.component';
import { TermsComponent } from './terms/terms.component';
import { PaymentTermsComponent } from './payment-terms/payment-terms.component';
import { PaymentComponent } from './payment/payment.component';
import { SelectCardComponent } from './payment/select-card/select-card.component';
import { SelectCreditComponent } from './payment/select-credit/select-credit.component';
import { PaymentSuccessComponent } from './payment/payment-success/payment-success.component';
import { PaymentOptionsComponent } from './payment/payment-options/payment-options.component';
import { SettingsComponent } from './settings/settings.component';
import { PricingComponent } from './pricing/pricing.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { WelcomeBackComponent } from './welcome-back/welcome-back.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { BlogComponent } from './blog/blog.component';
import { BlogArticleComponent } from './blog-article/blog-article.component';
import { CompetitionComponent } from './competition/competition.component';
import { BlogVideoComponent } from './blog-video/blog-video.component';
import { GroupedJobsComponent } from './grouped-jobs/grouped-jobs.component';
import { JobCardComponent } from './grouped-jobs/job-card/job-card.component';
import { CoursesComponent } from './courses/courses.component';
import { LiveChatComponent } from './live-chat/live-chat.component';
import { ForumComponent } from './forum/forum.component';

export const COMPONENTS = [
  LiveChatComponent,
  AboutComponent,
  ForumComponent,
  AccountManagementComponent,
  CoursesComponent,
  SelectCreditComponent,
  SelectCardComponent,
  PasswordResetComponent,
  BlogArticleComponent,
  BlogComponent,
  JobCardComponent,
  GroupedJobsComponent,
  ContactComponent,
  CookieComponent,
  DashboardComponent,
  PaymentComponent,
  FaqComponent,
  ForgotPasswordComponent,
  HomeComponent,
  JobBoardComponent,
  PricingComponent,
  BlogVideoComponent,
  PaymentOptionsComponent,
  JobBoardItemComponent,
  PaymentTermsComponent,
  WelcomeBackComponent,
  JobAdzunaCardComponent,
  JobBehiredCardComponent,
  SettingsComponent,
  SigninComponent,
  PrivacyComponent,
  PaymentSuccessComponent,
  SignupComponent,
  TermsComponent,
  NotFoundComponent,
  CompetitionComponent
];
