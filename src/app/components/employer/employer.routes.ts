import { MetaGuard } from '@ngx-meta/core';
import { Routes } from '@angular/router';

// Layouts:
import { SidebarLayout } from '../../layouts/sidebar/sidebar.layout';

// Components:
import { EmployerApplicationsComponent } from './employer-applications/employer-applications.component';
import { EmployerJobComponent } from './employer-job/employer-job.component';
import { EmployerJobsComponent } from './employer-jobs/employer-jobs.component';
import { EmployerProfileComponent } from './employer-profile/employer-profile.component';
import { EmployerCourseComponent } from './employer-course/employer-course.component';

// Guards:
import { AuthGuard } from '../../guards/auth.guard';
import { CreditGuard } from '../../guards/credit.guard';

const TITLE = 'Behired |';

export const ROUTES: Routes = [
  {
    path: '',
    canActivateChild: [ MetaGuard ],
    component: SidebarLayout,
    canActivate: [ AuthGuard ],
    children: [
      {
        path: 'profile',
        component: EmployerProfileComponent,
        data: { title: `${ TITLE } Profile`,
          meta: {
            title: `${ TITLE } Profile`,
            description: 'Behired profile'
          }
        }
      },
      {
        path: 'courses',
        component: EmployerCourseComponent,
        data: { title: `${ TITLE } Course`,
          meta: {
            title: `${ TITLE } Course`,
            description: 'Behired course'
          }
        }
      },
      {
        path: 'jobs',
        component: EmployerJobsComponent,
        data: { title: `${ TITLE } Jobs`,
          meta: {
            title: `${ TITLE } Jobs`,
            description: 'Behired jobs'
          }
        }
      },
      {
        path: 'applications/:jobId',
        component: EmployerApplicationsComponent,
        data: { title: `${ TITLE } Job`,
          meta: {
            title: `${ TITLE } Job`,
            description: 'Behired Job'
          }
        }
      },
      {
        path: 'job',
        component: EmployerJobComponent,
        canActivate: [ CreditGuard ],
        data: { title: `${ TITLE } Create a Job`,
          meta: {
            title: `${ TITLE } Create a Job`,
            description: 'Behired create a job today!'
          }
        }
      },
      {
        path: 'job/edit/:jobId',
        component: EmployerJobComponent,
        data: { title: `${ TITLE } Edit`,
          meta: {
            title: `${ TITLE } Edit`,
            description: 'Behired edit job and re post'
          }
        }
      }
    ]
  }
];
