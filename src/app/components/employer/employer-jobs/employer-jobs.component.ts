import { Component, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';
import { Router } from '@angular/router';

import { JobModel } from '../../../models/job.model';
import { JobService } from '../../../services/job.service';
import { LinkTrackingService } from '../../../services/link-tracking.service';

@Component({
  selector: 'employer-jobs-view',
  templateUrl: './employer-jobs.component.html'
})
export class EmployerJobsComponent implements OnInit {
  public jobs: Array<JobModel> = null;
  public isViewReady = false;

  constructor(
    private jobService: JobService,
    private linkTrackingService: LinkTrackingService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getJobs();
  }

  public getJobs(): void {
    this.jobService.getEmployersJobs()
      .pipe(
        take(1)
      ).subscribe((jobs) => {
        this.jobs = jobs;
        this.getJobStats(jobs);
      });
  }

  public getJobStats(jobs: Array<JobModel>): void {
    jobs.forEach(job => {
      this.linkTrackingService.getLink(job)
        .pipe(
          take(1)
        ).subscribe(data => {
          job['stats'] = (data !== null) ? data : false;
        });
    });

    this.isViewReady = true;
  }

  public editJob(job: JobModel): void {
    this.router.navigate([ `employer/job/edit/${ job.id }` ]);
  }
}
