import { Component, Inject, OnInit, OnDestroy, PLATFORM_ID } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject, Subscription } from 'rxjs';
import { takeUntil, flatMap } from 'rxjs/operators';
import { isPlatformBrowser } from '@angular/common';

import { GoogleApiService } from '../../../services/google-api.service';
import { EmployerModel } from '../../../models/employer.model';
import { FileService } from '../../../services/file.service';
import { StorageService } from '../../../services/storage.service';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'employer-profile-view',
  templateUrl: './employer-profile.component.html',
  styleUrls: ['./employer-profile.component.scss']
})
export class EmployerProfileComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public isHovering = false;
  public isUpdated = false;
  public logo: string;
  public uid: string;
  public uploadingLogo = false;
  public destroyed$: Subject<boolean> = new Subject<boolean>();
  public isViewReady = false;
  public options = { types: [], componentRestrictions: { country: 'GB' } };
  public profile: object | boolean = false;
  public isFileUploadError = false;
  public statusChanges: Subscription;
  public isErrorsVisible = false;
  public isGoogleReady = false;

  constructor(
    private fileService: FileService,
    private googleApiService: GoogleApiService,
    private formBuilder: FormBuilder,
    private storageService: StorageService,
    private userService: UserService,
    @Inject(PLATFORM_ID) private platform: object
  ) {}

  ngOnInit(): void {
    this.createForm();
    this.getProfile();
    this.formListener();
    this.enableGoogleAutoComplete();
  }

  ngOnDestroy(): void {
    this.destroyed$.next(false);
    this.destroyed$.complete();
  }

  public enableGoogleAutoComplete(): void {
    if (isPlatformBrowser(this.platform)) {
      this.isGoogleReady = true;
    }
  }

  public formListener(): void {
    this.statusChanges = this.form.statusChanges.subscribe(() => this.isErrorsVisible = false);
  }

  public getProfile(): void {
    this.uid = this.storageService.getUid();
    this.userService.getEmployerProfile()
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe((profile: EmployerModel) => {
        if (profile) {
          this.profile = (profile.address && profile.address.latitude) ? profile : false;
          this.form.patchValue(profile);

          if (profile.logo) {
            this.getFile();
          }
        }
        setTimeout(() => this.isViewReady = true, 100);
      });
  }

  public toggleHover(isHovering: boolean) {
    this.isHovering = isHovering;
  }

  public handleAddressChange(address: any): void {
    this.form.patchValue({
      address: {
        addressLine1: address.formatted_address,
        placeId: address.place_id
      }
    });
  }

  public createForm(): void {
    const phoneRegex = /^\d{4}[-\s]?\d{3}[-\s]?\d{4}$/;

    this.form = this.formBuilder.group({
      employerId: [ , [ ]],
      email: ['', [ ]],
      companyName: [ , [ Validators.required ]],
      contactName: [ , [ Validators.required ]],
      address: this.formBuilder.group({
        addressLine1: [ , [ ]],
        placeId: [ null, [ Validators.required ]],
        longitude: [ , []],
        latitude: [ , []]
      }),
      about: [ , [ ]],
      phoneNumber: [, [ Validators.pattern(phoneRegex) ]],
      logo: [ , [ ]],
      website: [ , [ ]],
      linkedIn: [ , [ ]]
    });
  }

  public uploadFile(event: FileList): void {
    const file = event.item ? event.item(0) : event['target'].files[0];

    if ((file.type === 'image/png' || file.type === 'image/jpg' || file.type === 'image/jpeg') && file.size <= 8000000) {
      const url = `logos/${ this.uid }/logo`;
      const uploading = this.fileService.uploadFile(url, file);
      this.uploadingLogo = true;

      uploading.snapshotChanges()
        .pipe(
          takeUntil(this.destroyed$)
        ).subscribe((data) => {
          if (data.bytesTransferred === data.totalBytes) {
            this.getFile(true);
          }
        });
    } else {
      this.isFileUploadError = true;
      setTimeout(() => this.isFileUploadError = false, 5000);
    }
  }

  public submit(isFileUpload?: boolean): void {
    if (this.form.valid) {
      const form = { ...this.form.value };

      this.googleApiService.getAddress(form.address.placeId)
        .pipe (
          takeUntil(this.destroyed$),
          flatMap((location) => {
            this.form.patchValue({
              address: {
                addressLine1: location['address'],
                latitude: location['lat'],
                longitude: location['lng']
              }
            });

            return this.userService.updateEmployerProfile(this.form.value);
          })
        ).subscribe(() => {
          this.isUpdated = true;
          setTimeout(() => this.isUpdated = false, 3000);

          this.getProfile();
        });
    } else {
      this.isErrorsVisible = true;
      window.scroll(0, 0);
    }

    if (isFileUpload) {
      this.userService.updateEmployerProfile(this.form.value);
    }
  }

  public getFile(isUpdate = false): void {
    const url = `logos/${ this.uid }/logo`;
    this.fileService.getFile(url, 'logo')
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe(logo => {
        this.logo = logo;
        this.form.patchValue({ logo });
        this.uploadingLogo = false;

        if (isUpdate) {
          this.submit(true);
        }
      }, error => setTimeout(() => this.getFile(isUpdate), 1000));
  }

  public deleteFile(): void {
    const url = `logos/${ this.uid }/logo`;

    this.fileService.deleteFile(url)
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe(() => {
        this.logo = null;
        this.form.patchValue({ logo: null });
        this.submit(true);
      });
  }
}
