import { CommonModule } from '@angular/common';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { NgModule } from '@angular/core';
import { NgxErrorsModule } from '@hackages/ngxerrors';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PreventDoubleSubmitModule } from 'ngx-prevent-double-submission';
import { TruncateModule } from 'ng2-truncate';
import { QuillModule } from 'ngx-quill';

import { ROUTES } from './employer.routes';

// Modules
import { SharedModule } from '../../shared/shared.module';
import { LayoutModule } from '../../layouts/layout.module';

// Components:
import { EmployerApplicationsComponent } from './employer-applications/employer-applications.component';
import { EmployerJobComponent } from './employer-job/employer-job.component';
import { EmployerJobsComponent } from './employer-jobs/employer-jobs.component';
import { EmployerProfileComponent } from './employer-profile/employer-profile.component';
import { EmployerCourseComponent } from './employer-course/employer-course.component';

@NgModule({
  imports: [
    QuillModule.forRoot({
      modules: {
        syntax: false,
        toolbar: [
          ['bold', 'italic', 'underline'],
          [{ list: 'ordered'}, { list: 'bullet' }],
          [{ header: [2, 3, 4, false] }],
        ]
      }
    }),
    LayoutModule,
    GooglePlaceModule,
    PreventDoubleSubmitModule.forRoot(),
    CommonModule,
    TruncateModule,
    FormsModule,
    NgxErrorsModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(ROUTES)
  ],
  declarations: [
    EmployerApplicationsComponent,
    EmployerJobComponent,
    EmployerJobsComponent,
    EmployerProfileComponent,
    EmployerCourseComponent
  ]
})
export class EmployerModule { }
