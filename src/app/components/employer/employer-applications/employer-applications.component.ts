import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { ApplicantModel } from '../../../models/applicant.model';
import { FileService } from '../../../services/file.service';
import { JobModel } from '../../../models/job.model';
import { JobService } from '../../../services/job.service';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'employer-applications-view',
  templateUrl: './employer-applications.component.html',
  styleUrls: [ './employer-applications.component.scss' ]
})
export class EmployerApplicationsComponent implements OnInit, OnDestroy {
  public destroyed$: Subject<boolean> = new Subject<boolean>();
  public applications: Array<object>;
  public applicant: ApplicantModel;
  public form: FormGroup;
  public isFormChangesActive = false;
  public isViewReady = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private fileService: FileService,
    private formBuilder: FormBuilder,
    private jobService: JobService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.routeListener();
    this.createForm();
    this.formListener();
  }

  ngOnDestroy(): void {
    this.destroyed$.next(false);
    this.destroyed$.complete();
  }

  public routeListener(): void {
    this.activatedRoute.params
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe(params => {
        if (params['jobId']) {
          this.getJob(params['jobId']);
        }
      });
  }

  public formListener(): void {
    this.form.valueChanges
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe(form => this.getApplicant({ key: form.applicant }));
  }

  public createForm(): void {
    this.form = this.formBuilder.group({
      applicant: [ null, [ ]]
    });
  }

  public getJob(jobId: string): void {
    this.jobService.getJobById(jobId)
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe((job: JobModel) => {
        this.applications = job.listOfApplications;

        if (this.applications && this.applications.length > 0) {
          this.getApplicant(this.applications[0]);
        } else {
          this.isViewReady = true;
        }
      });
  }

  public getApplicant(selectedApplicant: object): void {
    this.userService.getApplicantProfileById(selectedApplicant['key'])
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe((applicant: ApplicantModel) => {
        if (applicant) {
          this.applicant = applicant;

          if (this.applicant.cv) {
            this.getFile(applicant, `cvs/${ applicant.applicantId }/cv`, 'cv');
          }
          if (this.applicant.logo) {
            this.getFile(applicant, `logos/${ applicant.applicantId }/logo`, 'logo');
          }
        } else {
          this.applicant = null;
        }
        this.isViewReady = true;
      });
  }

  public getFile(applicant: ApplicantModel, url: string, type: string): void {
    this.fileService.getFile(url, type)
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe(link => applicant[type] = link);
  }
}
