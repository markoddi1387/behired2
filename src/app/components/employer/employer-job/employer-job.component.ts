import { ActivatedRoute, Router } from '@angular/router';
import { Component, Inject, OnInit, OnDestroy, PLATFORM_ID } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { flatMap, takeUntil, mergeMap } from 'rxjs/operators';
import { isPlatformBrowser } from '@angular/common';
import { DOCUMENT } from '@angular/common';

import { APP } from '../../../constants/app.constants';
import { EmailService } from '../../../services/email.service';
import { GoogleApiService } from '../../../services/google-api.service';
import { JobModel } from '../../../models/job.model';
import { JobService } from '../../../services/job.service';
import { EmployerModel } from './../../../models/employer.model';
import { TransactionService } from '../../../services/transaction.service';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'employer-job-view',
  templateUrl: './employer-job.component.html',
  styleUrls: [ './employer-job.component.scss' ]
})
export class EmployerJobComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public jobOptions = APP;
  public featuredCreditTotal: number;
  public isUpdateJob = false;
  public isUpdateJobFeatured = false;
  public cancelConfirmIsVisible = false;
  public updateConfirmIsVisible = false;
  public destroyed$: Subject<boolean> = new Subject<boolean>();
  public isUpdated = false;
  public submitConfirmIsVisible = false;
  public statusChanges: Subscription;
  public isErrorsVisible = false;
  public jobKey: string | null = null;
  public profile: EmployerModel;
  public job: JobModel;
  public options = { types: [], componentRestrictions: { country: 'GB' } };
  public message = 'Job saved successfully';
  public confirmMessage = 'Your job posting will go live';
  public cancelMessage = 'Your job posting will be cancelled and will not appear on the job board';
  public updateMessage = 'Your job posting will be updated and will appear on the job board';
  public formTags = [];
  public isFreeTier = true;
  public isGoogleReady = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private emailService: EmailService,
    private formBuilder: FormBuilder,
    private googleApiService: GoogleApiService,
    private jobService: JobService,
    private transactionService: TransactionService,
    private router: Router,
    private userService: UserService,
    @Inject(PLATFORM_ID) private platform: any,
    @Inject(DOCUMENT) private document: any
  ) {}

  ngOnInit(): void {
    this.domainType();
    this.createForm();
    // this.getCurrentCredit();
    this.getProfile();
    this.routeListener();
    this.formListener();
    this.enableGoogleAutoComplete();
  }

  ngOnDestroy(): void {
    this.destroyed$.next(false);
    this.destroyed$.complete();
  }

  public domainType(): void {
    const isPoolCoverDomain = this.document.location.hostname.includes('thepoolcover');

    if (isPoolCoverDomain) {
      this.jobOptions.sectors = [ 'Swimming Teacher', 'Lifeguard', 'Coach' ];
    }
  }

  public enableGoogleAutoComplete(): void {
    if (isPlatformBrowser(this.platform)) {
      this.isGoogleReady = true;
    }
  }

  public formListener(): void {
    this.statusChanges = this.form.statusChanges.subscribe(() => this.isErrorsVisible = false);
  }

  public getCurrentCredit(): void {
    this.transactionService.getCredits()
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe((credit) =>  {
        this.featuredCreditTotal = credit.featuredTotal ? credit.featuredTotal : 0;
      });
  }

  public routeListener(): void {
    this.activatedRoute.params
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe(params => {
        if (params['jobId']) {
          this.jobKey = params['jobId'];
          this.setJob();
        }
      });
  }

  public setFormTags(tags: Array<string>): void {
    this.formTags = tags;
  }

  public setJob(): void {
    this.jobService.getJobById(this.jobKey)
      .pipe(
        takeUntil(this.destroyed$),
      ).subscribe((job: JobModel) => {
        this.job = job;
        this.job['cancelCounter'] = !job.cancelCounter ? 0 : job.cancelCounter;
        this.isUpdateJob = true;
        this.isUpdateJobFeatured = job.isFeaturedJobSelected;
        this.form.patchValue(job);
        this.formTags = job.tags;
      });
  }

  public createForm(): void {
    const specialCharacterRegex = /^[^~!@#$%\^*_+={}[\]\\;"<>]*$/;

    this.form = this.formBuilder.group({
      name: [ , [ Validators.required, Validators.minLength(5), Validators.pattern(specialCharacterRegex) ]],
      summary: [ , [ Validators.required, Validators.minLength(15), Validators.pattern(specialCharacterRegex) ]],
      description: [ , [ Validators.required, Validators.minLength(30) ]],
      address: this.formBuilder.group({
        addressLine1: [ null, [ ]],
        placeId: [ null, [ Validators.required ]],
        longitude: [ , []],
        latitude: [ , []]
      }),
      employmentType: [ null, [ Validators.required ]],
      sector: [ null, [ Validators.required ]],
      payType: [ null, [ Validators.required ]],
      pay: [ , [ Validators.required ]],
      isFeaturedJobSelected: [ false, [ ]],
      tags: [ [], []],
      listOfApplications: [ null, []],
      datePosted: [ null, []],
      isActive: [],
      jobType: [ 'behired', [] ],
      aboutCompany: [ , [] ],
      website: [ , [] ]
    });
  }

  public reset(): void {
    this.form.reset();
  }

  public setTags(): boolean {
    if (this.formTags.length === 0) {
      const { name, sector, payType } = this.form.value;
      const nameArray = name.toLowerCase().split(' ').map(item => item.trim());
      const sectorArray = sector.toLowerCase().split(' ').map(item => item.trim());
      this.form.patchValue({ tags: [ name.toLowerCase(), sector.toLowerCase(), ...nameArray, ...sectorArray, payType.toLowerCase() ]});
    } else {
      const { name, sector } = this.form.value;

      if (this.formTags.includes(name.toLowerCase())) {
        this.form.patchValue({ tags: [ ...this.formTags ]});
      } else {
        this.form.patchValue({ tags: [ name.toLowerCase(), sector.toLowerCase(), ...this.formTags ]});
      }
    }

    return true;
  }

  public setFeatureCredit(): boolean {
    const { isFeaturedJobSelected } = this.form.value;
    return isFeaturedJobSelected && this.featuredCreditTotal > 0;
  }

  public createJob(): void {
    this.submitConfirmIsVisible = false;

    if (this.form.valid) {
      this.setTags();

      if (!this.isFreeTier) {
        this.form.patchValue({ datePosted: new Date(), isFeaturedJobSelected: this.setFeatureCredit() });
      } else {
        this.form.patchValue({ datePosted: new Date(), isFeaturedJobSelected: false });
      }

      this.googleApiService.getAddress(this.form.value.address.placeId)
        .pipe (
          flatMap(geometry => this.jobService.createJob(this.form.value, geometry)),
          flatMap(() => this.emailService.emailEmployerJobCreated(this.form.value.name)),
          takeUntil(this.destroyed$)
        ).subscribe(() => {
          if (!this.isFreeTier) {
            this.chargeCredit();
          } else {
            this.router.navigate(['./employer/jobs']);
          }
        });
    } else {
      this.isErrorsVisible = true;
      window.scroll(0, 0);
    }
  }

  public chargeCredit(): void {
    this.transactionService.getCredits()
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe((credit) =>  {
        // Note: Charge featured job
        const { isFeaturedJobSelected } = this.form.value;
        let featuredTotal = credit.featuredTotal;

        if (isFeaturedJobSelected && featuredTotal > 0) {
          featuredTotal = featuredTotal - 1;
        }

        // Note: Charge job credit
        const creditTotal = credit.creditTotal - 1;
        this.updateCredit(creditTotal, featuredTotal, credit.cardId);
      });
  }

  public updateCredit(creditTotal: any, featuredTotal: any, cardId: any): void {
    this.transactionService.updateCredits(creditTotal, featuredTotal, cardId)
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe(() => this.router.navigate(['./employer/jobs']));
  }

  public updateJob(): void {
    this.updateConfirmIsVisible = false;

    if (this.form.valid) {
      this.setTags();
      this.form.patchValue({ datePosted: new Date() });

      this.googleApiService.getAddress(this.form.value.address.placeId)
        .pipe (
          mergeMap(geometry => this.jobService.updateJob(this.form.value, this.jobKey, geometry)),
          flatMap(() => this.emailService.emailEmployerJobUpdated(this.form.value.name)),
          takeUntil(this.destroyed$)
        ).subscribe(() => {
          this.message = 'Job updated successfully';
          this.isUpdated = true;
          setTimeout(() => this.isUpdated = false, 3000);
       });
    } else {
      this.isErrorsVisible = true;
      window.scroll(0, 0);
    }
  }

  public cancelJob(): void {
    this.cancelConfirmIsVisible = false;

    this.jobService.cancelJob(this.jobKey)
      .pipe (
        mergeMap(() => this.emailService.emailCancelledJob(this.profile.email, this.job.name)),
        takeUntil(this.destroyed$)
      ).subscribe(() => {
        this.message = 'Job cancelled successfully';
        this.isUpdated = true;
        setTimeout(() => this.router.navigate(['./employer/jobs']), 3000);
      });
  }

  public handleAddressChange(address: any): void {
    this.form.patchValue({
      address: {
        addressLine1: address.formatted_address,
        placeId: address.place_id
      }
    });
  }

  public getProfile(): void {
    this.userService.getEmployerProfile()
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe((profile) => {
        if (profile) {
          profile['about'] = '';
          profile['website'] = '';
          this.profile = profile;
          this.form.patchValue({ aboutCompany: profile.about, website: profile.website });
        }
      });
  }
}
