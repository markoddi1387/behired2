import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { take } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { CompetitionService } from '../../services/competition.service';

@Component({
  selector: 'competition-view',
  templateUrl: './competition.component.html'
})
export class CompetitionComponent implements OnInit, OnDestroy {
  public isApplySuccess = false;
  public type: string;
  public isErrorsVisible = false;
  public form: FormGroup;
  public statusChanges: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private competitionService: CompetitionService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.createForm();
    this.routeListener();
    this.formListener();
  }

  ngOnDestroy() {
    this.statusChanges.unsubscribe();
  }

  public formListener(): void {
    this.statusChanges = this.form.statusChanges.subscribe(() => this.isErrorsVisible = false);
  }

  public routeListener(): void {
    this.activatedRoute.params
      .pipe(
        take(1)
      ).subscribe(params => {
        this.type = params['type'];
      });
  }

  public createForm(): void {
    this.form = this.formBuilder.group({
      email: [ , [
        Validators.email,
        Validators.required
      ]]
    });
  }

  public onSubmit(): void {
    const { email } = this.form.value;

    if (this.form.valid) {
      this.competitionService.apply(email, 'swimming')
        .pipe(
          take(1)
        ).subscribe(() => {
          this.isApplySuccess = true;
        });
    } else {
      this.isErrorsVisible = true;
    }
  }
}
