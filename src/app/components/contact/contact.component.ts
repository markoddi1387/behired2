import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { take, map } from 'rxjs/operators';
import { Subscription } from 'rxjs';

import { EmailService } from '../../services/email.service';

@Component({
  selector: 'contact-view',
  templateUrl: './contact.component.html'
})
export class ContactComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public success = false;
  public isErrorsVisible = false;
  public statusChanges: Subscription;
  public error: boolean | string;

  constructor(
    private emailService: EmailService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.createForm();
    this.formListener();
  }

  ngOnDestroy() {
    this.statusChanges.unsubscribe();
  }

  public formListener(): void {
    this.statusChanges = this.form.statusChanges.subscribe(() => this.isErrorsVisible = false);
  }

  public createForm(): void {
    this.form = this.formBuilder.group({
      name: [, [ Validators.required ]],
      email: [, [
        Validators.email,
        Validators.required,
        Validators.pattern('^([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\-])+\\.)+([a-zA-Z0-9]{2,4})+$')
      ]],
      enquiry: [, [ Validators.required ]]
    });
  }

  public onSubmit(): void {
    if (this.form.valid) {
      const { name, email, enquiry } = this.form.value;

      this.emailService.emailContact(name, email, enquiry)
        .pipe(
          take(1),
          map(() => this.success = true)
        ).subscribe(() => this.form.reset());
    } else {
      this.isErrorsVisible = true;
    }
  }
}
