import { Component, OnInit, OnDestroy } from '@angular/core';
import { take } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { ForumService } from '../../services/forum.service';
import { AuthService } from '../../services/auth.service';
import { ForumModel, ForumCommentModel } from '../../models/forum.model';

@Component({
  selector: 'forum-view',
  templateUrl: './forum.component.html',
  styleUrls: ['./forum.component.scss']
})
export class ForumComponent implements OnInit, OnDestroy {

  public form: FormGroup;
  public isLoginModalVisible = false;
  public forums: Array<ForumModel> = [];
  public sub: Subscription;
  public currentForum: ForumModel = null;
  public forumComments: Array<ForumCommentModel> = [];

  constructor(
    private forumService: ForumService,
    private authService: AuthService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.authCheck();
    this.createForm();
  }

  public authCheck(): void {
    this.sub = this.authService.getUser()
      .subscribe((user) => {
        this.isLoginModalVisible = (user === null);
        this.getForums();
      });
  }

  public createForm(): void {
    this.form = this.formBuilder.group({
      comment: [ '' , [ Validators.required, Validators.minLength(3) ] ]
    });
  }

  public getForums(): void {
    this.sub = this.forumService.getForums()
      .subscribe((Forums: Array<ForumModel>) => {
        this.forums = Forums;

        if (this.forums.length > 0) {
          this.getForumComments(this.forums[0]);
        }
      });
  }

  public getForumComments(currentForum: ForumModel): void {
    this.currentForum = currentForum;

    this.sub = this.forumService.getForumComments(this.currentForum.id)
      .subscribe((comments: Array<ForumCommentModel>) => {
        if (comments.length) {
          this.sortByDate(comments);
        } else {
          this.forumComments = [];
        }
      });
  }

  public vote(dir: string, commentItem: ForumCommentModel): void {
    if (dir === 'up') {
      commentItem.vote++;
    } else {
      if (commentItem.vote > 0) {
        commentItem.vote--;
      }
    }

    this.sub = this.forumService.updateComment(commentItem)
      .subscribe();
  }

  public flag(commentItem: ForumCommentModel): void {
    commentItem.flag = true;

    this.sub = this.forumService.updateComment(commentItem)
      .subscribe();
  }

  public addComment(): void {
    if (this.form.valid) {
      const { comment } = this.form.value;
      this.form.patchValue({ comment: '' });

      this.sub = this.forumService.addComment(this.currentForum, comment)
        .subscribe(() => this.getForumComments(this.currentForum));
    }
  }

  public sortByDate(comments: Array<ForumCommentModel>): void {
    const sortArray = comments;

    sortArray.sort((a, b) => {
      return new Date(b.date.toDate()).getTime() - new Date(a.date.toDate()).getTime();
    });

    this.forumComments = sortArray;
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}
