import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { JobModel } from '../../../models/job.model';
import { LinkTrackingService } from '../../../services/link-tracking.service';

@Component({
  selector: 'job-card',
  templateUrl: './job-card.component.html',
  styleUrls: ['./job-card.component.scss']
})
export class JobCardComponent implements OnInit {
  @Input() public job: JobModel;
  @Input() public isMapEnabled = true;
  @Input() public tabsEnabled = true;
  @Input() public index = 0;

  constructor(
    private router: Router,
    private linkTrackingService: LinkTrackingService
  ) {}

  ngOnInit(): void {
    this.job = this.job.name ? this.job : this.job['d'];
  }

  public goto(job: JobModel): void {
    this.linkTrackingService.setLink(job);
    this.router.navigateByUrl(`/job/${ job.id }`);
  }
}
