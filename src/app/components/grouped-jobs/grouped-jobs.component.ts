import { Component, OnInit } from '@angular/core';
import { take, map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

import { JobService } from '../../services/job.service';
import { EmployerModel } from '../../models/employer.model';

@Component({
  selector: ' grouped-jobs-view',
  templateUrl: './grouped-jobs.component.html',
  styleUrls: ['./grouped-jobs.component.scss']
})
export class GroupedJobsComponent implements OnInit {
  public listOfJobs = [];
  public employerId: string | null = null;
  public companyInfo: EmployerModel | null = null;

  constructor(
    private jobService: JobService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.getEmployerId();
    this.getAllJobs();
  }

  public getEmployerId(): void {
    this.activatedRoute.params.pipe(
      take(1)
    ).subscribe(params => {
      if (params['employerId']) {
        this.employerId = params['employerId'];
      }
    });
  }

  public getAllJobs(): void {
    this.jobService.getAllBehiredJobs()
      .pipe(
        take(1),
        map(querySnapshot => {
         return querySnapshot.map(value => {
           const job = value.payload.doc.data()['d'];
           job['id'] = value.payload.doc.id;
           return job;
          });
        })
      ).subscribe(jobs => {
        if (this.employerId === null) {
          this.listOfJobs = jobs;
        } else {
          this.listOfJobs = jobs.filter(job => job.employerId === this.employerId);

          if (this.listOfJobs.length) {
            this.companyInfo =  this.listOfJobs[0];
          }
        }
      });
  }
}
