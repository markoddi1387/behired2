import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { BlogModel, BlogCommentModel } from '../../models/blog.model';
import { BlogService } from '../../services/blog.service';
import { JobModel } from '../../models/job.model';
import { AuthService } from '../../services/auth.service';
import { JobService } from '../../services/job.service';
import { LOOKUP } from '../../constants/blog-lookup.constants';

@Component({
  selector: 'blog-article-view',
  templateUrl: './blog-article.component.html',
  styleUrls: ['./blog-article.component.scss']
})
export class BlogArticleComponent implements OnInit, OnDestroy {

  public form: FormGroup;
  public isViewReady = false;
  public isLoginModalVisible = false;
  public isUserLoggedIn = false;
  public blogArticle: BlogModel;
  public blogComments: Array<BlogCommentModel> = [];
  public blogName: string;
  public sub: Subscription;
  public listOfJobs: Array<JobModel>;
  public blogUrl: string;

  constructor(
    private blogService: BlogService,
    private jobService: JobService,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.createForm();
    this.listener();
    this.getSimilarJobs();
    this.authCheck();
  }

  public authCheck(): void {
    this.sub = this.authService.getUser()
      .subscribe((user) => this.isUserLoggedIn = (user === null));
  }

  public listener(): void {
    this.activatedRoute.params
      .pipe(
        take(1)
      ).subscribe(params => {
        if (params['id']) {
          const selectedBlog = LOOKUP[params['id']];

          if (selectedBlog) {
            this.blogUrl = `https://behired.co.uk/blog/${ params['id'] }`;
            this.getBlogArticle(selectedBlog.id, selectedBlog.name);
          } else {
            this.router.navigate(['/blog']);
          }
        }
      });
  }

  public createForm(): void {
    this.form = this.formBuilder.group({
      comment: [ '' , [ Validators.required, Validators.minLength(3) ] ]
    });
  }

  public getComments(): void {
    this.sub = this.blogService.getComments(this.blogArticle.guid)
      .subscribe((comments: Array<BlogCommentModel>) => {
        if (comments.length) {
          this.sortByDate(comments);
        } else {
          this.blogComments = [];
        }
    });
  }

  public addComment(): void {
    if (this.form.valid) {
      this.isLoginModalVisible = false;

      if (this.isUserLoggedIn) {
        this.isLoginModalVisible = true;
      } else {
        const { comment } = this.form.value;
        this.form.patchValue({ comment: '' });

        this.sub = this.blogService.addComment(this.blogArticle, comment)
          .subscribe(() => this.getComments());
      }
    }
  }

  public getBlogArticle(blogId: string, blogName: string): void {
    this.blogName = blogName;
    this.blogService.getBlogById(blogName, blogId)
      .pipe(
        take(1)
      ).subscribe((blog: BlogModel) => {
        this.blogArticle = blog;
        this.isViewReady = true;
        this.getComments();
      });
  }

  public getSimilarJobs(): void {
    this.jobService.getAllActiveBehiredJobsByLimit()
      .pipe(
        take(1)
      ).subscribe((jobs: Array<JobModel>) => this.listOfJobs = jobs);
  }

  public sortByDate(comments: Array<BlogCommentModel>): void {
    const sortArray = comments;

    sortArray.sort((a, b) => {
      return new Date(b.date.toDate()).getTime() - new Date(a.date.toDate()).getTime();
    });

    this.blogComments = sortArray;
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}
