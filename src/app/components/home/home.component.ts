import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { isPlatformBrowser } from '@angular/common';
import { DOCUMENT } from '@angular/common';

import { APP } from '../../constants/app.constants';
import { GoogleApiService } from '../../services/google-api.service';

@Component({
  selector: 'home-view',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public form: FormGroup;
  public options = { types: ['(cities)'], componentRestrictions: { country: 'GB' } };
  public domain: string;
  public isRecruiter = false;
  public isPoolcover = false;
  public searchOptions = APP;
  public isGoogleReady = false;

  constructor(
    @Inject(DOCUMENT) private document: any,
    private formBuilder: FormBuilder,
    private router: Router,
    private googleApiService: GoogleApiService,
    @Inject(PLATFORM_ID) private platform: object
  ) {}

  ngOnInit(): void {
    this.createForm();
    this.setDomain();
  }

  public createForm(): void {
    this.form = this.formBuilder.group({
      term: [ , []],
      postCode: [ , []],
      postCodeDisplay: [, []],
      radius: [ ]
    });
  }

  public setDomain(): void {
    this.domain = this.document.location.hostname.split('.')[0];
    this.isRecruiter = this.document.location.hostname.includes('recruiters');
    this.isPoolcover = this.document.location.hostname.includes('thepoolcover');

    if (isPlatformBrowser(this.platform)) {
      this.isGoogleReady = true;
    }
  }

  public handleAddressChange(address: any): void {
    this.form.patchValue({
      postCode: address.place_id,
      postCodeDisplay: address.name
    });
  }

  public onSubmit(): void {
    const { term, postCode, postCodeDisplay } = this.form.value;
    let { radius } = this.form.value;

    this.googleApiService.clickEvent('search', term);

    if (postCode === null && postCodeDisplay === null && term === null) {
      this.router.navigate([ 'job-board' ]);
    }

    if (postCode === null && postCodeDisplay !== null && term === null) {
      this.router.navigate([ 'job-board', postCodeDisplay ]);
    }

    if (postCode === null && postCodeDisplay !== null && term !== null) {
      this.router.navigate([ 'job-board', term ]);
    }

    if (postCode !== null && postCodeDisplay !== null && postCodeDisplay.length > 0) {
      if (radius === null) {
        radius = 5;
      }

      const searchTerm = (term && term.length > 0) ? term : ' ';
      this.router.navigate([ 'job-board', searchTerm, postCode, postCodeDisplay, radius ]);
    }

    if (postCode === null && postCodeDisplay === null && term !== null) {
      this.router.navigate([ 'job-board', term ]);
    }
  }
}
