import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject, forkJoin } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { takeUntil, take } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

import { AuthService } from '../../services/auth.service';
import { EmailService } from '../../services/email.service';
import { UserService } from '../../services/user.service';
import { StorageService } from '../../services/storage.service';
import { TransactionService } from '../../services/transaction.service';
import { RouterService } from '../../services/router.service';

@Component({
  selector: 'signup-view',
  templateUrl: './signup.component.html'
})
export class SignupComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public error: boolean | string;
  public passwordType = 'password';
  public type = 'applicant';
  public statusChanges: Subscription;
  public isRecruiter = false;
  public destroyed$: Subject<boolean> = new Subject<boolean>();
  public isErrorsVisible = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private emailService: EmailService,
    private formBuilder: FormBuilder,
    private routerService: RouterService,
    private userService: UserService,
    private storageService: StorageService,
    private transactionService: TransactionService
  ) {}

  ngOnInit(): void {
    this.isRecruiterListener();
    this.createForm();
    this.formListener();
  }

  public isRecruiterListener(): void {
    this.activatedRoute.params.pipe(
      take(1)
    ).subscribe(params => this.isRecruiter = params['recruiter']);
  }

  public formListener(): void {
    this.statusChanges = this.form.statusChanges
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe(() => this.isErrorsVisible = false);
  }

  ngOnDestroy(): void {
    this.destroyed$.next(false);
    this.destroyed$.complete();
    this.statusChanges.unsubscribe();
  }

  public createForm(): void {
    const passwordRegex = '(?!^[0-9]*$)(?!^[a-zA-Z]*$)^(.{8,15})$';

    this.form = this.formBuilder.group({
      email: [, [
        Validators.email,
        Validators.required
      ]],
      password: [, [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(15),
        Validators.pattern(passwordRegex)
      ]],
      confirmPassword: [, [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(15),
        Validators.pattern(passwordRegex)
      ]],
      type: [ '', []]
    });

    if (this.isRecruiter) {
      this.type = 'employer';
    }
  }

  public typeToggle(type: string): void {
    this.type = type;
  }

  public onSubmit(): void {
    const { password, confirmPassword } = this.form.value;
    this.error = false;

    if (this.form.valid && password === confirmPassword) {
      this.signUp();
    } else {
      this.isErrorsVisible = true;
      this.error = 'Please make sure your passwords match';
    }
  }

  private signUp(): void {
    const { email, password } = this.form.value;

    this.authService.signup(email, password, this.type)
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe(data => {
        if (data.user) {
          this.addProfile(this.type, email, data.user.uid);
        } else {
          this.error = data['message'];
        }
    });
  }

  public togglePasswordEye(): void {
    this.passwordType = (this.passwordType === 'password') ? 'text' : 'password';
  }

  private addProfile(type: string, email: string, uid: string): void {
    const registeredDate = new Date();

    switch (type) {
      case 'employer':
        forkJoin ([
          this.storageService.setUid(uid),
          this.transactionService.createCredit(0, 0),
          this.userService.addEmployerProfile({ email, registeredDate })
        ]).pipe(
          takeUntil(this.destroyed$)
        ).subscribe(() => {
          this.sendSignUpEmails(email);
        });
        break;
      default:
        forkJoin([
          this.storageService.setUid(uid),
          this.userService.addApplicantProfile({ email, registeredDate })
        ]).pipe(
          takeUntil(this.destroyed$)
        ).subscribe(() => this.sendSignUpEmails(email));
    }
  }

  private sendSignUpEmails(email: string): void {
    this.emailService.emailWelcome(email)
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe(() => this.routerService.redirectProfile());
  }
}
