import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, Observable, of, Subscription } from 'rxjs';
import { take, flatMap, takeUntil, map } from 'rxjs/operators';

import { ApplicationService } from '../../services/application.service';
import { AuthService } from '../../services/auth.service';
import { JobService } from '../../services/job.service';
import { ApplicantModel } from '../../models/applicant.model';
import { UserService } from '../../services/user.service';
import { JobModel } from '../../models/job.model';

@Component({
  selector: 'job-board-item',
  templateUrl: './job-board-item.component.html',
  styleUrls: [ './job-board-item.component.scss' ]
})
export class JobBoardItemComponent implements OnInit, OnDestroy {
  public destroyed$: Subject<boolean> = new Subject<boolean>();
  public isViewReady = false;
  public selectedJob: JobModel;
  public listOfJobs: Array<JobModel>;
  public applyMessage: string;
  public isAppliedSuccess: boolean;
  public isLoggedIn: boolean;
  public selectedJobKey: string;
  public isAppliedFailed: boolean;
  public applyConfirmIsVisible = false;
  public applyConfirmMessage = `By clicking yes you will be applying immediately, however, you may want to update your profile before applying`;
  public routeListener: Subscription;

  constructor(
    private applicationService: ApplicationService,
    private authService: AuthService,
    private activatedRoute: ActivatedRoute,
    private jobService: JobService,
    private router: Router,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.jobListener();
    this.verifyLogin();
  }

  ngOnDestroy(): void {
    this.destroyed$.next(false);
    this.destroyed$.complete();
  }

  public jobListener(): void {
    this.activatedRoute.params.pipe(
      takeUntil(this.destroyed$)
    ).subscribe(params => {
      if (params['id']) {
        this.selectedJobKey = params['id'];
        this.getJob(params['id']);
      }
    });
  }

  public verifyLogin(): void {
    this.authService.getUser()
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe(isLoggedIn => this.isLoggedIn = (isLoggedIn !== null));
  }

  public getJob(id: string): void {
    this.jobService.getExternalJobById(id)
      .pipe(
        takeUntil(this.destroyed$),
        map(data => data['d'])
      )
      .subscribe((job: JobModel) => {
        this.selectedJob = job;
        this.selectedJob.key = this.selectedJobKey;
        this.isViewReady = true;

        this.getSimilarJobs();
      });
  }

  public getSimilarJobs(): void {
    this.jobService.getAllActiveBehiredJobsByLimit()
      .pipe(
        take(1)
      ).subscribe((jobs: Array<JobModel>) => this.listOfJobs = jobs);
  }

  public applyResponseStatus(response: object): Observable<any> {
    this.applyMessage = response['message'];

    if (response['isApplicable']) {
      this.isAppliedSuccess = true;
      setTimeout(() => this.isAppliedSuccess = false, 4000);
    } else {
      this.isAppliedFailed = true;
      setTimeout(() => this.isAppliedFailed = false, 4000);
    }

    return of({});
  }

  public applicantLoggedOut(): void {
    this.router.navigate(['/signin/apply']);
  }

  public applyVerify(job: JobModel): void {
    this.applyConfirmIsVisible = false;

    if (this.isLoggedIn) {
      this.userService.getApplicantProfile()
        .pipe (
          flatMap(applicant => this.applicationService.isApplicantApplicable(job, applicant)),
          take(1)
        ).subscribe((response) => {
          if (response['verifiedApplicant'].isApplicable) {
            this.apply(job, response['applicant']);
          } else {
            this.applyResponseStatus(response['verifiedApplicant']);
          }
        });
    } else {
      this.applicantLoggedOut();
    }
  }

  public apply(job: JobModel, applicant: ApplicantModel): void {
    this.applicationService.applyForJob(job, applicant)
      .pipe (
        take(1),
        flatMap(() => this.applyResponseStatus({ isApplicable: true, message: 'Application Successful' }))
      ).subscribe(() => this.sendApplyEmails(job, applicant));
  }

  public sendApplyEmails(job: JobModel, applicant: ApplicantModel): void {
     this.applicationService.applyEmails(job, applicant)
      .pipe (
        take(2)
      ).subscribe();
  }
}
