import { Component, Input, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';

import { JobModel } from '../../../models/job.model';
import { LinkTrackingService } from '../../../services/link-tracking.service';

@Component({
  selector: 'job-adzuna-card',
  templateUrl: './job-adzuna-card.component.html',
  styleUrls: ['./job-adzuna-card.component.scss']
})
export class JobAdzunaCardComponent implements OnInit {
  @Input() public job: JobModel;

  constructor(
    private linkTrackingService: LinkTrackingService
  ) {}

  ngOnInit(): void {
    this.job = this.job.name ? this.job : this.job['d'];
  }

  public trackJob(job: JobModel): void {
    this.linkTrackingService.setLink(job);
    window.open(job.url, '_blank');
  }
}
