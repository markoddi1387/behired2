import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { JobModel } from '../../../models/job.model';
import { LinkTrackingService } from '../../../services/link-tracking.service';

@Component({
  selector: 'job-behired-card',
  templateUrl: './job-behired-card.component.html',
  styleUrls: ['./job-behired-card.component.scss']
})
export class JobBehiredCardComponent implements OnInit {
  @Input() public job: JobModel;
  @Input() public isMapEnabled = true;
  @Input() public tabsEnabled = true;
  @Input() public index = 0;
  @Input() public isSaved = false;

  constructor(
    private router: Router,
    private linkTrackingService: LinkTrackingService
  ) {}

  ngOnInit(): void {
    this.job = this.job.name ? this.job : this.job['d'];
  }

  public goto(job: JobModel): void {
    this.linkTrackingService.setLink(job);
    this.router.navigateByUrl(`/job/${ job.id }`);
  }
}
