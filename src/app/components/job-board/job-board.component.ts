import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, HostListener, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import * as _ from 'lodash';

import { JobService } from '../../services/job.service';
import { GoogleApiService } from '../../services/google-api.service';
import { APP } from '../../constants/app.constants';
import { JobModel } from '../../models/job.model';
import { SavedJobService } from '../../services/saved-job.service';
import { SearchLookupService } from '../../services/search-lookup.service';

@Component({
  selector: 'job-board-view',
  templateUrl: './job-board.component.html',
  styleUrls: ['./job-board.component.scss']
})
export class JobBoardComponent implements OnInit {

  public form: FormGroup;
  public numberOfSavedJobsFound = 0;
  public totalNumberOfJobs = 0;
  public isLoadMoreVisible = false;
  public isViewSavedJobs = false;
  public isFiltersVisible = false;
  public isJobsLoaded = false;
  public isScrollTopVisible = false;
  public lastItemBehired = null;
  public lastItemAdzuna = null;
  public listOfJobs: Array<JobModel> = [];
  public listOfSavedJobs: Array<JobModel> = [];
  public featuredJobs: Array<JobModel> = [];
  public searchOptions = APP;
  public options = { types: ['(cities)'], componentRestrictions: { country: 'GB' } };
  public destroyed$: Subject<boolean> = new Subject<boolean>();
  public selectedFilterTypes = { sectors: '', payTypes: '', brands: '', term: '' };
  public geo = { lat: null, lng: null, radius: 5 };
  public isGoogleReady = false;

  @HostListener('window:scroll', ['$event'])
  onWindowScroll() {
    const pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
    const max = document.documentElement.scrollHeight;

    if (pos === max && this.isLoadMoreVisible) {
      this.loadMoreJobs();
    }

    this.isScrollTopVisible = pos > 1500;
  }

  constructor(
    private activatedRoute: ActivatedRoute,
    private jobService: JobService,
    private formBuilder: FormBuilder,
    private location: Location,
    private googleApiService: GoogleApiService,
    private savedJobService: SavedJobService,
    private searchLookupService: SearchLookupService,
    @Inject(PLATFORM_ID) private platform: object
  ) {}

  ngOnInit(): void {
    this.createForm();
    this.listener();
    this.setFilters();
    this.enableGoogleAutoComplete();
  }

  public enableGoogleAutoComplete(): void {
    if (isPlatformBrowser(this.platform)) {
      this.isGoogleReady = true;
    }
  }

  public scrollTop(): void {
    if (isPlatformBrowser(this.platform)) {
      window.scroll(0, 0);
    }
  }

  public handleAddressChange(address: any): void {
    let { radius } = this.form.value;
    const coords = address.geometry.location;

    if (radius === null) {
      radius = 20;
      this.form.patchValue({ radius });
    }

    this.geo = { lat: coords.lat(), lng: coords.lng(), radius };

    this.form.patchValue({
      postCode: address.place_id,
      postCodeDisplay: address.name
    });
  }

  public setFilters(): void {
    this.isFiltersVisible = window.innerWidth > 1074;
  }

  public listener(): void {
    this.activatedRoute.params
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe(params => {
        if (params['term']) {
          this.setSearchValue(params);
        } else {
          this.getAllJobs('basic', true, false, () => {});
        }
      });
  }

  public getJobCount(): void {
    this.jobService.getCount()
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe(data => {
        this.totalNumberOfJobs = data.adzuna.count + data.behired.count;
      });
  }

  public createForm(): void {
    this.form = this.formBuilder.group({
      term: [ ],
      postCode: [ ],
      postCodeDisplay: [],
      radius: [ ]
    });
  }

  public reset(): void {
    this.listOfJobs = [];
    this.lastItemBehired = null;
    this.lastItemAdzuna = null;
    this.isLoadMoreVisible = true;
    this.isViewSavedJobs = false;
    this.selectedFilterTypes = { sectors: '', payTypes: '', brands: '', term: '' };
    this.location.replaceState(this.location.path().split('/')[0] + '/job-board', '');
    this.geo = { lat: null, lng: null, radius: 20 };

    this.createForm();
    this.getAllJobs('basic', false, false, () => {});
  }

  public getAllJobs(queryType: string, isNewSearch: boolean, isPagination: boolean, callback: () => void): void {
    this.isJobsLoaded = false;

    this.jobService.getJobs(queryType, this.selectedFilterTypes, this.geo, this.lastItemBehired, this.lastItemAdzuna, isPagination)
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe(data => {
        const jobs =  [ ...data.behiredJobs, ...data.adzunaJobs ];

        if (isNewSearch) {
          this.listOfJobs = jobs;
        } else {
          this.listOfJobs = [ ...this.listOfJobs, ...jobs ];
        }

        this.lastItemBehired = data.lastItemBehired ? data.lastItemBehired : null;
        this.lastItemAdzuna = data.lastItemAdzuna ? data.lastItemAdzuna : null;

        this.isLoadMoreVisible = true;

        if (data.adzunaJobs.length === 0 || queryType === 'geo') {
          this.isLoadMoreVisible = false;
        }

        if (queryType === 'geo') {
          this.sortByDate();
          this.isJobsLoaded = true;
        } else {
          this.isJobsLoaded = true;
        }

        callback();
      });
  }

  public loadMoreJobs(): void {
    this.getAllJobs('basic', false, true, () => {});
  }

  public searchByFilter(filteredTerm: string, filterType: string): void {
    this.selectedFilterTypes[filterType] = (this.selectedFilterTypes[filterType] === filteredTerm) ? '' : filteredTerm;
    this.search(true);
  }

  public search(isNewSearch: boolean): void {
    const { postCode, radius } = this.form.value;
    let { term } = this.form.value;
    const isPostCodeActive = postCode && postCode.length > 0;

    this.googleApiService.clickEvent('search', term);

    if (term && term.length > 0) {
      term = this.searchLookupService.findSearchTerm(term);
      this.selectedFilterTypes['term'] = term;
    }

    if (isPostCodeActive) {
      this.geo.radius = radius;
      this.getAllJobs('geo', isNewSearch, false, () => {});
    } else {
      if (this.isAnyFiltersSelected(term)) {
        this.getAllJobs('basic', isNewSearch, false, () => {});
      }
    }
  }

  public isAnyFiltersSelected(term: string | null): boolean {
    const termSelected = term === null || term.length === 0;

    return !(
      termSelected &&
      this.selectedFilterTypes.sectors === '' &&
      this.selectedFilterTypes.payTypes === '' &&
      this.selectedFilterTypes.brands === '' &&
      this.selectedFilterTypes.term === '' &&
      this.listOfJobs.length !== 0
    );
  }

  public setSearchValue(params: any): void {
    const { postCode, postCodeDisplay } = params;
    let { term, radius } = params;
    radius = radius ? radius : 5;

    if (term.trim().length === 0) {
      term = null;
    } else {
      term = term.toLowerCase();
      term = this.searchLookupService.findSearchTerm(term);
    }

    this.form.patchValue({
      term,
      postCode,
      postCodeDisplay,
      radius
    });

    if (postCode) {
      this.googleApiService.getAddress(postCode)
        .pipe(
          takeUntil(this.destroyed$)
        ).subscribe(geometry => {
          this.geo = { lat: geometry['lat'], lng: geometry['lng'], radius };
          this.search(true);
        });
    } else {
      this.search(true);
    }
  }

  public getSavedJobs(): void {
    this.isViewSavedJobs = !this.isViewSavedJobs;

    if (this.isViewSavedJobs) {
      this.savedJobService.getAllSavedJobs()
        .pipe(
          takeUntil(this.destroyed$)
        ).subscribe(data => {
          this.listOfSavedJobs = data;

          if (this.listOfSavedJobs.length === 0) {
            this.isViewSavedJobs = false;
          }
        });
    }
  }

  public sortByDate(): void {
    const sortArray = this.listOfJobs;

    sortArray.sort((a, b) => {
      return new Date(b.datePosted.toDate()).getTime() - new Date(a.datePosted.toDate()).getTime();
    });

    this.listOfJobs = sortArray;
  }
}
