import { Component } from '@angular/core';
import { take } from 'rxjs/operators';

import { AuthService } from '../../services/auth.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'pricing-view',
  templateUrl: './pricing.component.html',
  styleUrls: [ './pricing.component.scss' ]
})
export class PricingComponent {
  public isEmployerLoggedIn = false;

  constructor(
    private authService: AuthService,
    private userService: UserService
  ) {
    this.isUserLoggedIn();
  }

  public isUserLoggedIn(): void {
    this.authService.getUser()
      .pipe(
        take(1)
      ).subscribe((user) => {
        if (user != null) {
          this.verifyProfileType();
        }
      });
  }

  public verifyProfileType(): void {
    this.userService.getEmployerProfile()
      .pipe(
        take(1)
      ).subscribe((user) => {
        if (user != null) {
          this.isEmployerLoggedIn = true;
        }
      });
  }
}
