import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { take } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Params } from '@angular/router';

import { AuthService } from '../../services/auth.service';
import { RouterService } from '../../services/router.service';

@Component({
  selector: 'account-management-view',
  templateUrl: './account-management.component.html'
})
export class AccountManagementComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public error: boolean | string;
  public mode: string;
  public isErrorsVisible = false;
  public code: string;
  public passwordType = 'password';
  public statusChanges: Subscription;

  constructor(
    private authService: AuthService,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private routerService: RouterService
  ) {}

  ngOnInit(): void {
    this.getParams();
    this.createForm();
    this.formListener();
  }

  ngOnDestroy() {
    this.statusChanges.unsubscribe();
  }

  public formListener(): void {
    this.statusChanges = this.form.statusChanges.subscribe(() => this.isErrorsVisible = false);
  }

  private getParams(): void {
    this.activatedRoute.queryParams
      .pipe(
        take(1)
      ).subscribe((params: Params) => {
        this.mode = params['mode'];
        this.code = params['oobCode'];
        this.updateEmaiVerified();
      });
  }

  private updateEmaiVerified(): void {
    if (this.mode === 'verifyEmail') {
      this.authService.emailVerified(this.code)
        .pipe(
          take(1)
        ).subscribe(() => this.routerService.redirectProfile());
    }
  }

  public togglePasswordEye(): void {
    this.passwordType = (this.passwordType === 'password') ? 'text' : 'password';
  }

  public createForm(): void {
    const passwordRegex = '(?!^[0-9]*$)(?!^[a-zA-Z]*$)^(.{8,15})$';

    this.form = this.formBuilder.group({
      password: [ , [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(16),
        Validators.pattern(passwordRegex)
      ]],
      confirmPassword: [ , [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(16),
        Validators.pattern(passwordRegex)
      ]]
    });
  }

  public onSubmit(): void {
    this.error = false;
    const { password, confirmPassword } = this.form.value;

    if (this.form.valid && password === confirmPassword) {
      this.authService.resetPassword(this.code, password)
        .pipe(
          take(1)
        ).subscribe(data => {
          if (!data) {
            this.routerService.redirectProfile();
          } else {
            this.error = data.message;
          }
        });
    } else {
      this.isErrorsVisible = true;
    }
  }
}
