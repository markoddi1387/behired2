import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { take } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { AuthService } from '../../services/auth.service';
import { StorageService } from '../../services/storage.service';
import { RouterService } from '../../services/router.service';

@Component({
  selector: 'signin-view',
  templateUrl: './signin.component.html',
  styleUrls: [ './signin.component.scss' ]
})
export class SigninComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public error: boolean | string;
  public isErrorsVisible = false;
  public isInfoBannerVisible = false;
  public isApplyBannerVisible = false;
  public isPostBannerVisible = false;
  public passwordType = 'password';
  public statusChanges: Subscription;

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private storageService: StorageService,
    private routerService: RouterService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.createForm();
    this.routeListener();
    this.formListener();
  }

  ngOnDestroy() {
    this.statusChanges.unsubscribe();
  }

  public formListener(): void {
    this.statusChanges = this.form.statusChanges.subscribe(() => this.isErrorsVisible = false);
  }

  public routeListener(): void {
    this.activatedRoute.params
      .pipe(
        take(1)
      ).subscribe(params => {
        this.isInfoBannerVisible = params['type'] === 'buy';
        this.isApplyBannerVisible = params['type'] === 'apply';
        this.isPostBannerVisible = params['type'] === 'post';
      });
  }

  public createForm(): void {
    const passwordRegex = '(?!^[0-9]*$)(?!^[a-zA-Z]*$)^(.{8,15})$';

    this.form = this.formBuilder.group({
      email: [ , [
        Validators.email,
        Validators.required
      ]],
      password: [, [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(15),
        Validators.pattern(passwordRegex)
     ]]
    });
  }

  public onSubmit(): void {
    const { email, password } = this.form.value;
    this.error = false;
    this.authService.setCurrentEmail = email;

    if (this.form.valid) {
      this.authService.login(email, password)
        .pipe(
          take(1)
        ).subscribe(data => {
          if (data.user) {
            this.storageService.setUid(data.user.uid);
            this.routerService.redirectProfile();
          } else {
            this.error = data['message'];
          }
        });
    } else {
      this.isErrorsVisible = true;
    }
  }

  public togglePasswordEye(): void {
    this.passwordType = (this.passwordType === 'password') ? 'text' : 'password';
  }
}
