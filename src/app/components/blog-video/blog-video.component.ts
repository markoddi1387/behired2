import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';

@Component({
  selector: 'blog-video-view',
  templateUrl: './blog-video.component.html'
})
export class BlogVideoComponent {
  public isPtVisible = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.listener();
  }

  public listener(): void {
    this.activatedRoute.params
      .pipe(
        take(1)
      ).subscribe(params => {
        if (params['id']) {
          const type = params['id'];

          if (type === 'pt' || type === 'personal-training') {
            this.isPtVisible = true;
          }
        }

        if (!this.isPtVisible) {
          this.router.navigate(['/blog']);
        }
      });
  }
}
