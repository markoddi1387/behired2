import { Component, OnInit, EventEmitter, Output, NgZone, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

declare var Stripe: any;

@Component({
  selector: 'select-card',
  templateUrl: './select-card.component.html',
  styleUrls: [ './select-card.component.scss' ]
})
export class SelectCardComponent implements OnInit {
  @Output() backStep: EventEmitter<number> = new EventEmitter<number>();
  @Output() pay: EventEmitter<object> = new EventEmitter<object>();
  @Input() public error = { message : '' };
  public paymentConfirmIsVisible = false;
  public confirmMessage = 'By clicking yes your payment will be proceessed immediately';
  public form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private zone: NgZone
  ) {}

  ngOnInit(): void {
    this.router.navigate(['/payment'], { queryParams: { id: 'step3' } });
    this.error['message'] = '';
    this.createForm();
  }

  public createForm(): void {
    this.form = this.formBuilder.group({
      cardNumber: [ '', [ Validators.required, Validators.minLength(14), Validators.maxLength(16) ] ],
      expiration: this.formBuilder.group({
        expirationMonth: [ '', [ Validators.required, Validators.minLength(2), Validators.maxLength(2) ] ],
        expirationYear: [ '', [ Validators.required, Validators.minLength(2), Validators.maxLength(2) ] ]
      }),
      securityNumber: [ '', [ Validators.required, Validators.minLength(3), Validators.maxLength(3) ] ]
    });
  }

  public back(): void {
    this.backStep.emit();
  }

  public validateCardDetails(): boolean {
    let isCardValid = true;
    const { cardNumber, expiration, securityNumber } = this.form.value;

    if (!Stripe.card.validateCVC(securityNumber)) {
      isCardValid = false;
      this.errorHandler('Please enter a valid cvc number');
    }

    if (!Stripe.card.validateExpiry(expiration.expirationMonth, expiration.expirationYear)) {
      isCardValid = false;
      this.errorHandler('Please enter a valid expiration date');
    }

    if (!Stripe.card.validateCardNumber(cardNumber)) {
      isCardValid = false;
      this.errorHandler('Please enter a valid card number');
    }

    return isCardValid;
  }

  public payment() {
    this.paymentConfirmIsVisible = false;
    this.error['message'] = '';

    if (this.validateCardDetails() && this.form.valid) {
      const { cardNumber, expiration, securityNumber } = this.form.value;

      Stripe.card.createToken({
        number: cardNumber,
        exp_month: expiration.expirationMonth,
        exp_year: expiration.expirationYear,
        cvc: securityNumber
      }, (status: number, response: any) => {
        this.zone.run(() => {
          this.stripeHandler(status, response);
        });
      });
    }
  }

  public stripeHandler(status: number, response: any): void {
    switch (status) {
      case 200:
        this.pay.emit(response);
        break;
      default:
        this.errorHandler(response.error.message);
    }
  }

  public errorHandler(message: string): void {
    this.error['message'] = message;
  }
}
