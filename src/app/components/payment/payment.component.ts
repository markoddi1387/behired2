import { Component, OnInit } from '@angular/core';
import { take, delay, filter, catchError } from 'rxjs/operators';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

import { PaymentService } from '../../services/payment.service';
import { TransactionService } from '../../services/transaction.service';

@Component({
  selector: 'payment-view',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
  public step = 'step1';
  public totalCredits = 0;
  public totalFeatured = 0;
  public jobPrice = 99;
  public featuredPrice = 25;
  public isProcessingPayment = false;
  public error = { message: '' };

  constructor(
    private transactionService: TransactionService,
    private paymentService: PaymentService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.routeListener();
  }

  public routeListener(): void {
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd)
      ).subscribe((event: NavigationEnd) => {
        if (this.activatedRoute.snapshot.queryParams.id !== undefined && this.activatedRoute.snapshot.queryParams.id !== 'credits') {
          this.step = this.activatedRoute.snapshot.queryParams.id;
        }
      });
  }

  public updateBasket(payment: any) {
    const { credit, featured } = payment;
    this.totalCredits = Number(credit);
    this.totalFeatured = Number(featured);
    this.step = 'step3';
  }

  public beginPayment(): void {
    this.step = 'step2';
    this.router.navigate(['/payment'], { queryParams: { id: 'step2' } });
  }

  public resetBasket(): void {
    this.step = 'step2';
    this.totalCredits = 0;
    this.totalFeatured = 0;
  }

  public makePayment(customer: object) {
    const token = customer['id'];
    const cardId = customer['card'].id;

    this.transactionService.getCredits()
      .pipe(
        take(1)
      ).subscribe((data) => this.payAndUpdateCredit(token, cardId, data.creditTotal, data.featuredTotal));
  }

  public payAndUpdateCredit(token: string, cardId: string, currentCreditTotal: string, currentFeatureTotal: string) {
    const newCreditTotal = this.totalCredits + Number(currentCreditTotal);
    const newFeaturedTotal = this.totalFeatured + Number(currentFeatureTotal);
    this.isProcessingPayment = true;

    this.paymentService.pay(token, this.totalCredits, this.totalFeatured)
      .pipe(
        take(1),
        delay(2000),
        catchError((error) => this.errorHandler(error))
      ).subscribe((data) => {
        if (data['isSuccess']) {
          this.transactionService.updateCredits(newCreditTotal, newFeaturedTotal, cardId)
            .pipe(
              take(1)
            ).subscribe(() => {
              this.step = 'step4';
              this.isProcessingPayment = false;
            });
        }
      });
  }

  private errorHandler(data: object): never {
    this.isProcessingPayment = false;
    this.error = { message: data['error'].Message.message };
    throw(data);
  }
}
