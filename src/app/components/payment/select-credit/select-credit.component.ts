import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'select-credit',
  templateUrl: './select-credit.component.html',
  styleUrls: [ './select-credit.component.scss' ]
})
export class SelectCreditComponent implements OnInit {
  @Output() changeCredit: EventEmitter<object> = new EventEmitter<object>();
  public form: FormGroup;
  public credits = [ 0, 1, 2, 3, 4, 5, 10, 15, 20, 50 ];
  public featuredCredits = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ];

  constructor(
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.createForm();
  }

  public createForm(): void {
    this.form = this.formBuilder.group({
      credit: [ null, [ ] ],
      featured: [ null, [ ] ]
    });
  }

  public onSubmit(): void {
    let { credit, featured } = this.form.value;

    if (credit === null) {
      credit = 0;
    }
    if (featured === null) {
      featured = 0;
    }
    if (Number(credit) > 0 || Number(featured) > 0) {
      this.changeCredit.emit({ credit, featured });
    }
  }
}
