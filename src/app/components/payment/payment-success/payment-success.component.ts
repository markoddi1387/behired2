import { Component } from '@angular/core';

@Component({
  selector: 'payment-success',
  templateUrl: './payment-success.component.html',
  styleUrls: [ './payment-success.component.scss' ]
})
export class PaymentSuccessComponent {
}
