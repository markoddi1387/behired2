import { Component, Output, EventEmitter } from '@angular/core';
import { take } from 'rxjs/operators';

import { EmailService } from './../../../services/email.service';
import { UserService } from './../../../services/user.service';

@Component({
  selector: 'payment-options',
  templateUrl: './payment-options.component.html',
  styleUrls: [ './payment-options.component.scss' ]
})
export class PaymentOptionsComponent {
  @Output() beginPayment: EventEmitter<any> = new EventEmitter<any>();
  public isInvoiceSuccess = false;

  constructor(
    private emailService: EmailService,
    private userService: UserService
  ) {}

  public sendInvoiceEmail(): void {
    this.userService.getEmployerProfile().pipe(
      take(1)
    ).subscribe((profile) => {
      this.emailService.emailInvoiceRequest(profile.email, profile.contactName).pipe(
        take(1)
      ).subscribe(() => this.isInvoiceSuccess = true);
    });
  }

  public makePayment(): void {
    this.beginPayment.emit();
  }
}
