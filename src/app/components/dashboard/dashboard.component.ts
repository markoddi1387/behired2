import * as Chart from 'chart.js';
import { Component, OnInit, OnDestroy, ElementRef, Input } from '@angular/core';

@Component({
  selector: 'dashboard-view',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  public chart: Chart;
  public ctx: ElementRef;

  constructor(private elementRef: ElementRef) {}

  ngOnInit(): void {
    this.createChart();
  }

  ngOnDestroy(): void {
    this.chart.destroy();
  }

  public createChart(): void {
    const ctx = this.elementRef.nativeElement.querySelector('#Chart');
    this.chart = new Chart(ctx, {
      type: 'doughnut',
      data: {
        labels: [ 'Title 1', 'Title 2' ],
        datasets: [
          {
            label: 'Title',
            backgroundColor: ['#2c3e50', '#2ca3af'],
            data: [40 , 60]
          }
        ]
      },
      options: {
        legend: {
          display: false
        }
      }
    });
  }
}
