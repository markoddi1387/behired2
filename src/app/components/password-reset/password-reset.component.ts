import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject, Subscription, forkJoin } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { AuthService } from '../../services/auth.service';
import { EmailService } from '../../services/email.service';

@Component({
  selector: 'password-reset-view',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.scss']
})
export class PasswordResetComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public isErrorsVisible = false;
  public statusChanges: Subscription;
  public isUpdated: boolean;
  public destroyed$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private emailService: EmailService
  ) {}

  ngOnInit(): void {
    this.createForm();
    this.formListener();
  }

  public formListener(): void {
    this.statusChanges = this.form.statusChanges.subscribe(() => this.isErrorsVisible = false);
  }

  ngOnDestroy(): void {
    this.destroyed$.next(false);
    this.destroyed$.complete();
    this.statusChanges.unsubscribe();
  }

  public createForm(): void {
    const passwordRegex = '(?!^[0-9]*$)(?!^[a-zA-Z]*$)^(.{8,15})$';

    this.form = this.formBuilder.group({
      password: [, [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(15),
        Validators.pattern(passwordRegex)
      ]],
      confirmPassword: [, [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(15),
        Validators.pattern(passwordRegex)
      ]],
    });
  }

  public submit(): void {
    if (this.form.valid) {
      const form = { ...this.form.value };

      forkJoin ([
        this.authService.changePassword(form.password),
        this.emailService.emailPasswordReset()
      ]).pipe (
        takeUntil(this.destroyed$)
      ).subscribe(() => {
        this.isUpdated = true;
        setTimeout(() => this.isUpdated = false, 5000);
      });
    } else {
      this.isErrorsVisible = true;
    }
  }
}
