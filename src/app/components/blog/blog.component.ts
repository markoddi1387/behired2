import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

import { BlogService } from '../../services/blog.service';

@Component({
  selector: 'blog-view',
  templateUrl: './blog.component.html'
})
export class BlogComponent {
  public listOfBehiredBlogs: Observable<any>;
  public listOfPoolcoverBlogs: Observable<any>;
  public personalTrainingUrl = 'https://firebasestorage.googleapis.com/v0/b/leisure-project.appspot.com/o/assets%2Fbackgrounds%2Fblog-pt.jpg?alt=media&token=7701fb7a-15f7-4298-9d1e-e0a3596a6fb5';

  constructor(
    private blogService: BlogService,
    private router: Router
  ) {
    this.getAllBlogs();
  }

  public getAllBlogs(): void {
    this.listOfBehiredBlogs = this.blogService.getAllBlogs('@behired');
    this.listOfPoolcoverBlogs = this.blogService.getAllBlogs('@thepoolcover');
  }

  public selectedBlog(title: string): void {
    const slug = title.replace(/[&\/\\#,+()$~%.'":——*?!<>{}]/g, '').replace(/\s+/g, '-').toLowerCase();
    this.router.navigate([`/blog/${ slug }`]);
  }
}
