import { Component, OnInit } from '@angular/core';
import { zip, Subject, forkJoin } from 'rxjs';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';

import { AuthService } from '../../services/auth.service';
import { JobService } from '../../services/job.service';
import { UserService } from '../../services/user.service';
import { TransactionService } from '../../services/transaction.service';

@Component({
  selector: 'settings-view',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  public isApplicant: boolean;
  public destroyed$: Subject<boolean> = new Subject<boolean>();
  public deleteConfirmIsVisible = false;
  public isFreeTier = true;
  public userId: string;
  public message = 'Deleting this account will mean all your data will be removed.';

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private jobService: JobService,
    private transactionService: TransactionService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getUserType();
  }

  public delete(): void {
    this.deleteConfirmIsVisible = false;
    (this.isApplicant) ? this.deleteApplicant() : this.deleteEmployer();
  }

  public cancelDelete(): void {
    this.deleteConfirmIsVisible = false;
  }

  public deleteApplicant(): void {
    this.userService.deleteApplicant(this.userId)
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe(() => this.deleteAuthAccount());
  }

  public deleteEmployer(): void {
    forkJoin([
      this.userService.deleteEmployer(this.userId),
      this.jobService.deleteByEmployerId(),
      this.transactionService.deleteCredit()
    ]).pipe(
      takeUntil(this.destroyed$)
    ).subscribe(() => {
      this.deleteAuthAccount();
    });
  }

  public deleteAuthAccount(): void {
    this.authService.deleteUser()
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe(() => this.logout());
  }

  public getUserType(): void {
    zip(
      this.userService.getApplicantProfile(),
      this.userService.getEmployerProfile()
    ).pipe(
      takeUntil(this.destroyed$)
    ).subscribe((users: object) => {
      this.isApplicant = users[0] !== null;
      this.userId = users[0] ? users[0].applicantId : users[1].employerId;
    });
  }

  public logout(): void {
    this.authService.logout()
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe(() => this.router.navigate(['/']));
  }
}
