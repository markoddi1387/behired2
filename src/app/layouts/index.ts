import { CenterLayout } from './center/center.layout';
import { HomeLayout } from './home/home.layout';
import { SidebarLayout } from './sidebar/sidebar.layout';
import { InfoLayout } from './info/info.layout';
import { CompetitionLayout } from './competition/competition.layout';
import { CoursesLayout } from './courses/courses.layout';
import { VolunteerLayout } from './volunteer/volunteer.layout';

export const LAYOUTS = [
  CenterLayout,
  SidebarLayout,
  HomeLayout,
  CompetitionLayout,
  InfoLayout,
  CoursesLayout,
  VolunteerLayout
];
