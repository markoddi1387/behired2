import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { take } from 'rxjs/operators';

@Component({
  selector: 'competition-layout',
  templateUrl: './competition.layout.html',
  styleUrls: ['./competition.layout.scss']
})
export class CompetitionLayout implements OnInit {
  public type: string;

  constructor(
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.listener();
  }

  public listener(): void {
    this.activatedRoute.params
      .pipe(
        take(1)
      ).subscribe(params => {
        this.type = params['type'];
      });
  }
}
