import { Component } from '@angular/core';

@Component({
  selector: 'full-width',
  templateUrl: './full-width.layout.html',
  styleUrls: ['./full-width.layout.scss']
})
export class FullWidthLayout {
  constructor() {}
}
