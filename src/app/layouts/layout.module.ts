import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// Components:
import { CenterLayout } from './center/center.layout';
import { CompetitionLayout } from './competition/competition.layout';
import { HomeLayout } from './home/home.layout';
import { InfoLayout } from './info/info.layout';
import { SidebarLayout } from './sidebar/sidebar.layout';
import { FullWidthLayout } from './full-width/full-width.layout';
import { CoursesLayout } from './courses/courses.layout';
import { VolunteerLayout } from './volunteer/volunteer.layout';

// Modules
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SharedModule
  ],
  declarations: [
    CenterLayout,
    CompetitionLayout,
    HomeLayout,
    InfoLayout,
    CoursesLayout,
    FullWidthLayout,
    SidebarLayout,
    VolunteerLayout
  ]
})
export class LayoutModule { }
