import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { take } from 'rxjs/operators';

@Component({
  selector: 'courses-layout',
  templateUrl: './courses.layout.html',
  styleUrls: ['./courses.layout.scss']
})
export class CoursesLayout implements OnInit {
  public type: string;

  constructor(
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.listener();
  }

  public listener(): void {
    this.activatedRoute.params
      .pipe(
        take(1)
      ).subscribe(params => {
        this.type = params['type'];
      });
  }
}
