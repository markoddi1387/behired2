import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'home-layout',
  templateUrl: './home.layout.html',
  styleUrls: [ './home.layout.scss' ]
})
export class HomeLayout implements OnInit {
  public className: string;
  public domain: string;

  constructor(@Inject(DOCUMENT) private document: any) {}

  ngOnInit(): void {
    this.setDomain();
  }

  public setDomain(): void {
    this.domain = this.document.location.hostname.split('.');

    if (this.domain[0] === 'recruiters') {
      this.className  = 'recruiter';
    }
  }
}
