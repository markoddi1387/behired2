import { Component } from '@angular/core';

@Component({
  selector: 'sidebar-layout',
  templateUrl: './sidebar.layout.html',
  styleUrls: ['./sidebar.layout.scss']
})
export class SidebarLayout {
  public isApplicant: boolean;
  public locked: boolean;

  constructor() {}

  public setApplicant(isApplicant: boolean) {
    this.isApplicant = isApplicant;
  }

  public setLocked(locked: boolean) {
    this.locked = locked;
  }
}
