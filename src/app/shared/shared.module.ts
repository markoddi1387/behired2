import { AgmCoreModule } from '@agm/core';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgxErrorsModule } from '@hackages/ngxerrors';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PreventDoubleSubmitModule } from 'ngx-prevent-double-submission';

// Angular Material
import { MatCommonModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { MatChipsModule } from '@angular/material/chips';
import { MatFormFieldModule } from '@angular/material/form-field';

import { HeaderExternalComponent } from './components/header-external/header-external.component';
import { FooterExternalComponent } from './components/footer-external/footer-external.component';
import { HeaderInternalComponent } from './components/header-internal/header-internal.component';
import { NotificationComponent } from './components/notification/notification.component';
import { SideNavComponent } from './components/side-nav/side-nav.component';
import { CookieNotificationComponent } from './components/cookie-notification/cookie-notification.component';
import { FormDialogComponent } from './components/form-dialog/form-dialog.component';
import { MapComponent } from './components/map/map.component';
import { WelcomeNotificationComponent } from './components/welcome-notification/welcome-notification.component';
import { MobileInternalHeaderComponent } from './components/mobile-internal-header/mobile-internal-header.component';
import { ConfirmNotificationComponent } from './components/confirm-notification/confirm-notification.component';
import { ProgressBarComponent } from './components/progress-bar/progress-bar.component';
import { SaveJobComponent } from './components/save-job/save-job.component';
import { NetworkNotificationComponent } from './components/network-notification/network-notification.component';
import { StatusNotificationComponent } from './components/status-notification/status-notification.component';
import { TagInputComponent } from './components/tag-input/tag-input.component';
import { RatingsSwitchComponent } from './components/ratings-switch/ratings-switch.component';
import { SubscribeBlogComponent } from './components/subscribe-blog/subscribe-blog.component';
import { ScoreModalComponent } from './components/score-modal/score-modal.component';
import { ReviewModalComponent } from './components/review-modal/review-modal.component';
import { SocialLoginComponent } from './components/social-login/social-login.component';

import { environment } from '../../environments/environment';

import { DIRECTIVES } from '../directives/index';
@NgModule({
  imports: [
    AgmCoreModule.forRoot({
      apiKey: environment.googleApiKey
    }),
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    NgxErrorsModule,
    PreventDoubleSubmitModule.forRoot(),
    MatFormFieldModule,
    MatChipsModule,
    MatCommonModule,
    MatInputModule
  ],
  declarations: [
    CookieNotificationComponent,
    FooterExternalComponent,
    HeaderExternalComponent,
    SocialLoginComponent,
    WelcomeNotificationComponent,
    ReviewModalComponent,
    ScoreModalComponent,
    RatingsSwitchComponent,
    MobileInternalHeaderComponent,
    StatusNotificationComponent,
    SubscribeBlogComponent,
    ProgressBarComponent,
    HeaderInternalComponent,
    NotificationComponent,
    NetworkNotificationComponent,
    ConfirmNotificationComponent,
    TagInputComponent,
    SideNavComponent,
    SaveJobComponent,
    FormDialogComponent,
    MapComponent,
    DIRECTIVES
  ],
  exports: [
    CookieNotificationComponent,
    FooterExternalComponent,
    HeaderExternalComponent,
    TagInputComponent,
    SaveJobComponent,
    HeaderInternalComponent,
    ProgressBarComponent,
    WelcomeNotificationComponent,
    StatusNotificationComponent,
    SocialLoginComponent,
    NetworkNotificationComponent,
    SubscribeBlogComponent,
    RatingsSwitchComponent,
    ReviewModalComponent,
    ScoreModalComponent,
    MobileInternalHeaderComponent,
    NotificationComponent,
    ConfirmNotificationComponent,
    SideNavComponent,
    FormDialogComponent,
    MapComponent,
    DIRECTIVES
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SharedModule { }
