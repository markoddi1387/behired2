import { Component, OnInit } from '@angular/core';
import { Network } from '@ngx-pwa/offline';

@Component({
  selector: 'network-notification',
  templateUrl: './network-notification.component.html',
})
export class NetworkNotificationComponent implements OnInit {
  public isVisible = false;

  constructor(protected network: Network) {}

  ngOnInit(): void {
    this.monitorNetwork();
  }

  public monitorNetwork(): void {
    this.network.onlineChanges
      .subscribe((isNetworkActive) => this.isVisible = !isNetworkActive);
  }
}
