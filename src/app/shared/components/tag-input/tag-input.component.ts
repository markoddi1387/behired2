import { Component, Input, EventEmitter, Output, OnInit, OnChanges } from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';

export interface Tag {
  name: string;
}

@Component({
  selector: 'tag-input',
  templateUrl: './tag-input.component.html',
  styleUrls: [ './tag-input.component.scss' ]
})
export class TagInputComponent implements OnInit, OnChanges {
  @Input() public formTags: Array<string>;
  @Output() updateFormTags = new EventEmitter();
  public visible = true;
  public selectable = true;
  public removable = true;
  public addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ ENTER, COMMA ];
  public tags: Tag[] = [];

  ngOnInit(): void {
    this.initTags();
  }

  ngOnChanges(): void {
    this.initTags();
  }

  public initTags(): void {
    this.tags = this.formTags.map(name => {
      return { name };
    });
  }

  public add(event: any): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.tags.push({ name: value.trim() });
    }

    if (input) {
      input.value = '';
    }

    this.emitTags();
  }

  public remove(tag: Tag): void {
    const index = this.tags.indexOf(tag);

    if (index >= 0) {
      this.tags.splice(index, 1);
    }

    this.emitTags();
  }

  public emitTags(): void {
    const mappedFormTags = this.tags.map((tag) => tag['name']);
    this.updateFormTags.emit(mappedFormTags);
  }
}
