import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, zip } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { ApplicantModel } from '../../../models/applicant.model';
import { AuthService } from '../../../services/auth.service';
import { UserService } from '../../../services/user.service';
import { NotificationService } from '../../../services/notification.service';


@Component({
  selector: 'mobile-internal-header',
  templateUrl: './mobile-internal-header.component.html',
  styleUrls: [ './mobile-internal-header.component.scss' ]
})
export class MobileInternalHeaderComponent implements OnInit, OnDestroy {
  public isApplicant: boolean;
  public locked = true;
  public applicant: ApplicantModel;
  public isMenuVisible = false;
  public destroyed$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private authService: AuthService,
    private router: Router,
    private userService: UserService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.listener();
    this.verifyUserType();
  }

  ngOnDestroy(): void {
    this.destroyed$.next(false);
    this.destroyed$.complete();
  }

  public listener(): void {
    this.notificationService.profileStatus$
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe(() => this.verifyUserType());
  }

  public verifyLockedMode(user: object): void {
    if (this.isApplicant) {
      this.locked = !(user['address'] && user['address'].placeId && user['firstName']);
    } else {
      this.locked = !(user['address'] && user['address'].placeId && user['contactName']);
    }
  }

  public verifyUserType(): void {
    zip(this.userService.getApplicantProfile(), this.userService.getEmployerProfile())
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe((users: object) => {
        const isProfileSetup = users[0] !== null || users[1] !== null;

        if (isProfileSetup) {
          this.isApplicant = users[0] !== null;
          const user = this.isApplicant ? users[0] : users[1];
          this.verifyLockedMode(user);
        }
      });
  }

  public logout(): void {
    this.authService.logout()
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe(() => this.router.navigate(['/']));
  }
}
