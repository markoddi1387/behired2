import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import * as moment from 'moment';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RatingsModel } from '../../../models/ratings.model';

@Component({
  selector: 'review-modal',
  templateUrl: './review-modal.component.html',
  styleUrls: [ './review-modal.component.scss' ]
})
export class ReviewModalComponent implements OnInit {

  @Output() cancel: EventEmitter<any> = new EventEmitter<any>();
  @Output() confirm: EventEmitter<any> = new EventEmitter<any>();
  public isErrorsVisible = false;
  public message: string;
  public form: FormGroup;
  public score;

  constructor(
    private formBuilder: FormBuilder
  ) {
    this.createForm();
  }

  ngOnInit(): void {
    this.score = new RatingsModel();
  }

  public createForm(): void {
    this.form = this.formBuilder.group({
      title: [ '' , [ Validators.required, Validators.minLength(5), Validators.maxLength(100) ]],
      review: [ '' , [ Validators.required, Validators.minLength(10), Validators.maxLength(300) ]],
      score: [ 0 , [ Validators.required ]]
    });
  }

  public setScore(selectedItem: any): void {
    this.form.patchValue({ score: selectedItem.value });

    this.score.service.map((stat: any) => {
      if (stat.value <= selectedItem.value) {
        stat.selected = true;
      } else {
        stat.selected = false;
      }
    });
  }

  public confirmAction(): void {
    this.isErrorsVisible = false;

    if (this.form.valid) {
      const review = this.form.value;
      review.owner = 'Anonymous';
      review.datePosted = moment().format('DD-MM-YYYY');
      this.confirm.emit(review);
    } else {
      this.isErrorsVisible = true;
    }
  }

  public cancelAction(): void {
    this.cancel.emit();
  }
}
