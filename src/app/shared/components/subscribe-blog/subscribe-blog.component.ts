import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { take } from 'rxjs/operators';

import { BlogService } from '../../../services/blog.service';

@Component({
  selector: 'subscribe-blog',
  templateUrl: './subscribe-blog.component.html',
  styleUrls: [ './subscribe-blog.component.scss' ]
})
export class SubscribeBlogComponent implements OnInit {
  public form: FormGroup;
  public isSuccessVisible = false;

  constructor(
    private formBuilder: FormBuilder,
    private blogService: BlogService
  ) {}

  ngOnInit(): void {
    this.createForm();
  }

  public createForm(): void {
    this.form = this.formBuilder.group({
      email: [ , [
        Validators.email,
        Validators.required
      ]]
    });
  }

  public onSubmit(): void {
    const { email } = this.form.value;

    if (this.form.valid) {
      this.blogService.subscribeToMailingList(email)
        .pipe(
          take(1)
        ).subscribe(() => this.isSuccessVisible = true);
    }
  }
}
