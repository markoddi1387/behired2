import { Component, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';

import { StorageService } from '../../../services/storage.service';

@Component({
  selector: 'cookie-notification',
  templateUrl: './cookie-notification.component.html',
})
export class CookieNotificationComponent implements OnInit {
  public isVisible = false;

  constructor(private storageService: StorageService) {}

  ngOnInit(): void {
    this.getCookie();
  }

  public setCookie(): void {
    this.isVisible = false;
    this.storageService.setCookie();
  }

  public getCookie(): void {
    this.storageService.getCookie()
      .pipe(
        take(1)
      ).subscribe(data => this.isVisible = data === null);
  }
}
