import { Component, OnInit, HostBinding } from '@angular/core';
import { take } from 'rxjs/operators';
import { zip } from 'rxjs';

import { UserService } from '../../../services/user.service';

@Component({
  selector: 'welcome-notification',
  templateUrl: './welcome-notification.component.html',
  styleUrls: [ './welcome-notification.component.scss' ]
})
export class WelcomeNotificationComponent implements OnInit {
  @HostBinding('class.hide') isVisible: boolean;
  public isViewReady = false;
  public isApplicant: boolean;

  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.verifyUserType();
  }

  public verifyUserType(): void {
    zip(this.userService.getApplicantProfile(), this.userService.getEmployerProfile())
      .pipe(
        take(2)
      ).subscribe((users: object) => {
        this.isApplicant = users[0] !== null;

        if (users[0] !== null || users[1] !== null) {
          if (this.isApplicant) {
            const user = users[0];
            this.isVisible = (user['address'] && user['address'].placeId && user['firstName']);
          } else {
            const user = users[1];
            this.isVisible = (user['address'] && user['address'].placeId && user['contactName']);
          }

          this.isViewReady = true;
        } else {
          this.isVisible = true;
          this.isViewReady = false;
        }
      });
  }
}
