import { Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.scss']
})
export class ProgressBarComponent implements OnChanges {
  @Input() public profile: any;
  @Input() public profileType: string;
  public progress: string;

  constructor() {}

  ngOnChanges(): void {
    setTimeout(() => {
      this.profileType === 'employer' ? this.setProgressBar(10) : this.setProgressBar(11);
    }, 1000);
  }

  public setApplicant(): void {
  }

  public setProgressBar(totalProperties: number): void {
    let propertiesCompleteCounter = 0;

    if (this.profile) {
      propertiesCompleteCounter = Object.keys(this.profile).length;
      let calc = (propertiesCompleteCounter / totalProperties * 100).toFixed(0);

      if (calc === '1') {
        calc = '100';
      }

      this.progress = `${ calc }%`;
    }
  }
}
