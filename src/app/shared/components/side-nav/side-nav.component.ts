import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, zip } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { ApplicantModel } from '../../../models/applicant.model';
import { AuthService } from '../../../services/auth.service';
import { UserService } from '../../../services/user.service';
import { NotificationService } from '../../../services/notification.service';

@Component({
  selector: 'side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit, OnDestroy {
  @Output() setApplicant: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() setLocked: EventEmitter<boolean> = new EventEmitter<boolean>();
  public isApplicant: boolean;
  public locked = true;
  public applicant: ApplicantModel;
  public isSideNavReady = false;
  public destroyed$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private authService: AuthService,
    private router: Router,
    private userService: UserService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.listener();
    this.verifyUserType();
  }

  ngOnDestroy(): void {
    this.destroyed$.next(false);
    this.destroyed$.complete();
  }

  public listener(): void {
    this.notificationService.profileStatus$
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe(() => this.verifyUserType());
  }

  public logout(): void {
    this.authService.logout()
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe(() => this.router.navigate(['/']));
  }

  public verifyLockedMode(user: object): void {
    if (this.isApplicant) {
      this.locked = !(user['address'] && user['address'].placeId && user['firstName']);
    } else {
      this.locked = !(user['address'] && user['address'].placeId && user['contactName']);
    }

    this.setLocked.emit(this.locked);
  }

  public verifyUserType(): void {
    zip(this.userService.getApplicantProfile(), this.userService.getEmployerProfile())
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe((users: object) => {
        const isProfileSetup = users[0] !== null || users[1] !== null;

        if (isProfileSetup) {
          this.isApplicant = users[0] !== null;
          const user = this.isApplicant ? users[0] : users[1];
          this.verifyLockedMode(user);
        }

        this.setApplicant.emit(this.isApplicant);
        this.isSideNavReady = true;
      });
  }
}
