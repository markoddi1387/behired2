import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, zip } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { AuthService } from '../../../services/auth.service';
import { TransactionService } from '../../../services/transaction.service';
import { NotificationService } from '../../../services/notification.service';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'header-internal',
  templateUrl: './header-internal.component.html',
  styleUrls: [ './header-internal.component.scss' ]
})
export class HeaderInternalComponent implements OnInit, OnDestroy {
  public email: string;
  public creditTotal: number;
  public featuredTotal: number;
  public destroyed$: Subject<boolean> = new Subject<boolean>();
  public isApplicant: boolean;
  public locked = true;
  public isFreeTier = true;
  public isViewReady = false;

  constructor(
    private authService: AuthService,
    private transactionService: TransactionService,
    private notificationService: NotificationService,
    private userService: UserService
  ) {
    this.creditListener();
    this.profileListener();
  }

  ngOnInit(): void {
    this.getUser();
    this.getCreditTotal();
    this.verifyUserType();
  }

  ngOnDestroy(): void {
    this.destroyed$.next(false);
    this.destroyed$.complete();
  }

  public creditListener(): void {
    this.notificationService.creditUpdate$
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe(() => {
        this.getCreditTotal();
        this.verifyUserType();
      });
  }

  public profileListener(): void {
    this.notificationService.profileStatus$
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe(() => this.verifyUserType());
  }

  public getUser(): void {
    this.authService.getUser()
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe((user) => {
        if (user !== null) {
          this.email = user.email;
        }
      });
  }

  public getCreditTotal(): void {
    this.transactionService.getCredits()
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe((credit) => {
        this.creditTotal = credit === null ? 0 : credit.creditTotal;
        this.featuredTotal = credit === null ? 0 : credit.featuredTotal || 0;
      });
  }

  public verifyUserType(): void {
    zip(
      this.userService.getApplicantProfile(),
      this.userService.getEmployerProfile()
    ).pipe(
        takeUntil(this.destroyed$)
      ).subscribe((users: object) => {
        const isProfileSetup = users[0] !== null || users[1] !== null;

        if (isProfileSetup) {
          this.isApplicant = users[0] !== null;
          const user = this.isApplicant ? users[0] : users[1];
          this.verifyLockedMode(this.isApplicant, user);
        }

        this.isViewReady = true;
      });
  }

  public verifyLockedMode(isApplicant: boolean, user: object): void {
    if (isApplicant) {
      this.locked = !(user['address'] && user['address'].placeId && user['firstName']);
    } else {
      this.locked = !(user['address'] && user['address'].placeId && user['contactName']);
    }
  }
}
