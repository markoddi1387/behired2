import { Component, OnDestroy, Inject } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil, filter } from 'rxjs/operators';
import { DOCUMENT } from '@angular/common';

import { AuthService } from '../../../services/auth.service';
import { RouterService } from '../../../services/router.service';

@Component({
  selector: 'header-external',
  templateUrl: './header-external.component.html'
})
export class HeaderExternalComponent implements OnDestroy {

  public isLoggedIn = false;
  public isRecruiter = false;
  public isFreeTier = false;
  public isMobileMenuVisible = false;
  public isWhiteText: boolean;
  public destroyed$: Subject<boolean> = new Subject<boolean>();
  public isViewReady = false;

  constructor(
    @Inject(DOCUMENT) private document: any,
    private authService: AuthService,
    private router: Router,
    private routerService: RouterService
  ) {
    this.routeListener();
    this.isAuthenticated();
  }

  ngOnDestroy(): void {
    this.destroyed$.next(false);
    this.destroyed$.complete();
  }

  public routeListener(): void {
    this.router.events
      .pipe(
        takeUntil(this.destroyed$),
        filter(event => (event instanceof NavigationStart || event instanceof NavigationEnd))
      ).subscribe((event: NavigationEnd) => {
        this.isMobileMenuVisible = false;
        this.isRecruiter = this.document.location.hostname.includes('recruiters');
        this.isFreeTier = this.document.location.hostname.includes('thepoolcover');

        this.isWhiteText = (event.url === '/home' || event.url === '/' || event.url.includes('competition')
        || event.url.includes('courses') || event.url.includes('volunteer') || event.url.includes('?fbclid'));
      });
  }

  public enterApp(): void {
    this.routerService.redirectJobs();
  }

  public goto(): void {
    window.location.href = 'https://recruiters.behired.co.uk';
  }

  private isAuthenticated(): void {
    this.authService.getUser()
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe((user) => {
        if (user !== null) {
          if (!user.isAnonymous) {
            this.isLoggedIn = true;
          }
        }

        this.isViewReady = true;
      });
  }
}
