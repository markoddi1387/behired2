import { Component, EventEmitter, Output, OnInit } from '@angular/core';

import { RatingsModel } from '../../../models/ratings.model';
import { StatsModel } from '../../../models/stats.model';

@Component({
  selector: 'score-modal',
  templateUrl: './score-modal.component.html',
  styleUrls: [ './score-modal.component.scss' ]
})
export class ScoreModalComponent implements OnInit {

  @Output() cancel: EventEmitter<any> = new EventEmitter<any>();
  @Output() confirm: EventEmitter<any> = new EventEmitter<any>();
  public message: string;
  public form;
  public stats: RatingsModel;

  constructor() {}

  ngOnInit(): void {
    this.stats = new RatingsModel();
    this.form = new StatsModel();
  }

  public setScore(selectedItem: any, type: string): void {
    this.form[type] = selectedItem.value;

    this.stats[type].map((stat: any) => {
      if (stat.value <= selectedItem.value) {
        stat.selected = true;
      } else {
        stat.selected = false;
      }
    });
  }

  public confirmAction(): void {
    this.confirm.emit(this.form);
  }

  public cancelAction(): void {
    this.cancel.emit();
  }
}
