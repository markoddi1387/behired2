import { Component, Input } from '@angular/core';
import { take } from 'rxjs/operators';

import { JobModel } from './../../../models/job.model';
import { SavedJobService } from './../../../services/saved-job.service';

@Component({
  selector: 'save-job',
  templateUrl: './save-job.component.html',
  styleUrls: [ './save-job.component.scss' ]
})
export class SaveJobComponent {
  @Input() public job: JobModel;
  public isSavedJob: boolean;

  constructor(private savedJobService: SavedJobService) {
    this.getSaveStatus();
  }

  public toggleSave(): void {
    this.isSavedJob ? this.deleteSavedJob() : this.saveJob();
  }

  public getSaveStatus(): void {
    this.savedJobService.getSavedJob()
      .pipe(
        take(1)
      ).subscribe((savedJobs) => this.isSavedJob = savedJobs && savedJobs[this.job.key] !== undefined);
  }

  public saveJob(): void {
    this.savedJobService.addSavedJob(this.job.key, this.job)
      .pipe(
        take(1)
        ).subscribe((data) => this.isSavedJob = data !== null);
  }

  public deleteSavedJob(): void {
    this.savedJobService.deleteSavedJob(this.job.key)
      .pipe(
        take(1)
      ).subscribe((data) => this.isSavedJob = false);
  }
}
