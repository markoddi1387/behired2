import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'confirm-notification',
  templateUrl: './confirm-notification.component.html',
  styleUrls: [ './confirm-notification.component.scss' ]
})
export class ConfirmNotificationComponent {
  @Input() public message: string;
  @Output() confirm: EventEmitter<any> = new EventEmitter<any>();
  @Output() cancel: EventEmitter<any> = new EventEmitter<any>();

  public confirmAction(): void {
    this.confirm.emit();
  }

  public cancelAction(): void {
    this.cancel.emit();
  }
}
