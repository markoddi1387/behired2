import { Component, Output, EventEmitter } from '@angular/core';
import { take } from 'rxjs/operators';

import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'social-login',
  templateUrl: './social-login.component.html',
  styleUrls: [ './social-login.component.scss' ]
})
export class SocialLoginComponent {

  @Output() confirm: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private authService: AuthService
  ) {}

  public facebookSignIn(): void {
  }

  public googleSignIn(): void {
  }

  public signInAnonymously(): void {
    this.authService.signInAnonymously()
      .pipe(
        take(1)
      ).subscribe(() => this.confirm.emit());
  }
}
