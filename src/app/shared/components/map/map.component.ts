import { Component, Input } from '@angular/core';

@Component({
  selector: 'map-view',
  templateUrl: './map.component.html',
})
export class MapComponent {
  @Input() public job: any;
  @Input() public zoom = 11;
  @Input() public height = '';
  @Input() public scrollwheel = false;
  @Input() public zoomControl = false;

  public icon = './assets/icons/map-marker.png';
}
