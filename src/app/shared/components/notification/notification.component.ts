import { Component, OnInit, OnDestroy } from '@angular/core';
import { map, takeUntil, flatMap, take } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { AuthService } from '../../../services/auth.service';
import { NotificationModel } from '../../../models/notification.model';
import { NotificationService } from '../../../services/notification.service';

@Component({
  selector: 'notification',
  templateUrl: './notification.component.html',
})
export class NotificationComponent implements OnInit, OnDestroy {
  private destroyed$: Subject<boolean> = new Subject<boolean>();
  public isVisible = false;
  public isLoggedIn = false;
  public message: string;

  constructor(
    private authService: AuthService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.isEmailVerified();
  }

  ngOnDestroy(): void {
    this.destroyed$.next(false);
    this.destroyed$.complete();
  }

  public resendEmailVerification(): void {
    this.authService.sendVerifyEmail()
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe();
  }

  public isEmailVerified(): void {
    this.notificationService.notifications$
      .pipe(
        takeUntil(this.destroyed$),
        flatMap((notification: NotificationModel) => {
          return this.authService.getUser()
            .pipe(
              map(auth => {
                return { notification, isLoggedIn: (auth !== null) };
              })
            );
        })
      ).subscribe(response => {
        (response.notification.type === 'session') ? this.message = 'Session expired, please sign in' : this.message = 'Please verify your email';
        setTimeout(() => this.isVisible = response.notification.isVisible, 2000);
        this.isLoggedIn = response.isLoggedIn;
      });
  }
}
