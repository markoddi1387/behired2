import { Component, Input } from '@angular/core';

@Component({
  selector: 'ratings-switch',
  templateUrl: './ratings-switch.component.html',
  styleUrls: [ './ratings-switch.component.scss' ]
})
export class RatingsSwitchComponent {

  @Input() rating: string;
}
