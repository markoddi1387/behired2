import { Component, Input } from '@angular/core';

@Component({
  selector: 'status-notification',
  templateUrl: './status-notification.component.html',
})
export class StatusNotificationComponent {
  @Input() public status: boolean;

  constructor() {}
}
