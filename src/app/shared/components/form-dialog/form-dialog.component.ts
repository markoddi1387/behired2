import { Component, Input } from '@angular/core';

@Component({
  selector: 'form-dialog',
  templateUrl: './form-dialog.component.html',
})
export class FormDialogComponent {
  @Input() public isVisible = false;
  @Input() public title = 'Error';
  @Input() public message = 'Update Saved';

  constructor() {}
}
