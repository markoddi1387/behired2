import { HttpErrorResponse, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { NotificationModel } from './models/notification.model';
import { NotificationService } from './services/notification.service';

@Injectable()
export class CoreInterceptor implements HttpInterceptor {
  constructor(private notificationService: NotificationService) {}

  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
    return next.handle(request)
      .pipe(
        catchError(error => {
          if (error instanceof HttpErrorResponse) {
            switch ((error).status) {
              case 400:
                return this.errorHandler(error);
              case 401:
                return this.errorHandler(error);
              case 402:
                return this.errorHandler(error);
              case 500:
                return this.errorHandler(error);
              case 404:
                return this.errorHandler(error);
            }
          }
        })
      );
  }

  private errorHandler(response: HttpErrorResponse): Observable<never> {
    console.table(response);
    this.notificationService.updateNotification(new NotificationModel('session', true));
    return throwError(response);
  }
}
