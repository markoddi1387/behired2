export class CommentModel {
  title: string;
  review: string;
  score: number;
  owner: string;
  datePosted: string;
}
