export class RatingsModel {
  service = [
    { selected: false, value: 5 },
    { selected: false, value: 4 },
    { selected: false, value: 3 },
    { selected: false, value: 2 },
    { selected: false, value: 1 }
  ];
  communication = [
    { selected: false, value: 5 },
    { selected: false, value: 4 },
    { selected: false, value: 3 },
    { selected: false, value: 2 },
    { selected: false, value: 1 }
  ];
  quality = [
    { selected: false, value: 5 },
    { selected: false, value: 4 },
    { selected: false, value: 3 },
    { selected: false, value: 2 },
    { selected: false, value: 1 }
  ];
  safety = [
    { selected: false, value: 5 },
    { selected: false, value: 4 },
    { selected: false, value: 3 },
    { selected: false, value: 2 },
    { selected: false, value: 1 }
  ];
  recommendToAFriend = [
    { selected: false, value: 5 },
    { selected: false, value: 4 },
    { selected: false, value: 3 },
    { selected: false, value: 2 },
    { selected: false, value: 1 }
  ];
  workExperience = [
    { selected: false, value: 5 },
    { selected: false, value: 4 },
    { selected: false, value: 3 },
    { selected: false, value: 2 },
    { selected: false, value: 1 }
  ];
  cleanliness = [
    { selected: false, value: 5 },
    { selected: false, value: 4 },
    { selected: false, value: 3 },
    { selected: false, value: 2 },
    { selected: false, value: 1 }
  ];

  constructor() {}
}
