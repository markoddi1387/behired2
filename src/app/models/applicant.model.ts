import { AddressModel } from './address.model';

export class ApplicantModel {
  email?: string;
  applicantId?: string;
  about = '';
  cover = '';
  address?: AddressModel;
  country?: string;
  firstName?: string;
  gender?: string;
  lastName?: string;
  mobileNumber?: string;
  homeNumber?: string;
  cv?: string;
  cvName?: string;
  logo?: string;
  listOfJobApplications = [];
  latitude?: string;
  longitude?: string;
  registeredDate?: any;
}
