export class LinkModel {
  id: string;
  count: number;
  name: string;
  jobType: string;
  url: string;

  constructor(id: string, name: string, jobType: string, url: string) {
    this.id = id;
    this.name = name;
    this.jobType = jobType;
    this.url = url;
    this.count = 1;
  }
}
