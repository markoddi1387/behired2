import { CommentModel } from './comment.model';
import { StatsModel } from './stats.model';

export class ReviewModel {
  logo = '';
  reviews?: Array<CommentModel>;
  title: string;
  stats: StatsModel = new StatsModel();
  jobs = 0;
  photos: Array<string> = [];
  flag = false;
  map = '';
  about = '';
  type: string;
  ownerId: string;
  isActive = true;
  phone = null;
  website = null;
  jobBoard = null;
  email = null;
}
