import { AddressModel } from './address.model';

export class EmployerModel {
  employerId?: string;
  email?: string;
  about?: string;
  address?: AddressModel;
  facebook?: string;
  contactName?: string;
  linkedin?: string;
  companyName?: string;
  phoneNumber?: string;
  twitter?: string;
  website?: string;
  logo?: string;
  registeredDate?: any;
  employerName?: string;
}
