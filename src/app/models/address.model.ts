export class AddressModel {
  addressLine1?: string;
  addressLine2?: string;
  country?: string;
  latitude?: string;
  longitude?: string;
  postCode?: string;
  region?: string;
  tags?: any;
  placeId?: string;
}
