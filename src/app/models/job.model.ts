import { AddressModel } from './address.model';

export class JobModel {
  id?: string;
  jobType: string;
  address?: AddressModel;
  datePosted?: any;
  dateLastUpdated?: any;
  validThrough?: string;
  description?: string;
  employerId?: string;
  employerEmail?: string;
  employerName?: string;
  employmentType?: string;
  summary?: string;
  key?: string;
  logo?: string;
  name: string;
  location?: string;
  pay?: any;
  brand?: string;
  payType?: string;
  sector?: string;
  tags?: Array<string>;
  latitude?: string;
  longitude?: string;
  resource?: string;
  listOfApplications = [];
  isExpand: boolean;
  isActive: boolean;
  isFeaturedJobSelected = false;
  website?: string;
  url?: string;
  expired: boolean;
  aboutCompany = '';
  cancelCounter = 0;
}
