export class ForumModel {
  id: string;
  title: string;
}

export class ForumCommentModel {
  id?: string;
  forumId: string;
  comment: string;
  vote = 0;
  dateDisplay: string;
  date: any;
  owner: string;
  replies: Array<ForumCommentModel>;
  flag = false;
}
