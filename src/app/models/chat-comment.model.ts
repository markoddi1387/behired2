export class ChatComentModel {
  id: string;
  chatId: string;
  description: string;
  author?: string;
}
