export class ChatModel {
  id: string;
  title: string;
  description: string;
  author?: string;
}
