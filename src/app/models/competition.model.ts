export class CompetitionModel {
  email: string;
  type: string;

  constructor(email: string, type: string) {
    this.email = email;
    this.type = type;
  }
}
