export class NotificationModel {
  isVisible: boolean;
  type: string;

  constructor(type: string, isVisible: boolean) {
    this.type = type;
    this.isVisible = isVisible;
  }
}
