export class BlogModel {
  guid?: string;
  title?: string;
  description?: string;
  thumbnail?: string;
  pubDate?: string;
}

export class BlogCommentModel {
  id?: string;
  forumId: string;
  comment: string;
  vote = 0;
  dateDisplay: string;
  date: any;
  owner: string;
  flag = false;
}
