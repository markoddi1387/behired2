import * as moment from 'moment';

export class CreditModel {
  employerUid: string;
  cardId: string;
  creditTotal: number;
  featuredTotal: number;
  timestamp: string;

  constructor(employerUid: string, featuredTotal: number, creditTotal: number, cardId = '') {
    this.employerUid = employerUid;
    this.cardId = cardId;
    this.featuredTotal = featuredTotal;
    this.creditTotal = creditTotal;
    this.timestamp = moment().format('DD-MM-YYYY');
  }
}
