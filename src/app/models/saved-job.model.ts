import { AddressModel } from './../models/address.model';

export class SavedJobModel {
  id: string;
  key: string;
  name: string;
  description: string;
  address: AddressModel;
  pay: any;
  payType: string;
  logo: string;
  summary: string;
  employerName: string;
  employerId: string;

  constructor(id: string, key: string, name: string, description: string, address: AddressModel,
              pay: any, payType: string, logo: string, summary: string, employerName: string, employerId: string) {
    this.id = id;
    this.key = key;
    this.name = name;
    this.description = description;
    this.address = address;
    this.pay = pay;
    this.payType = payType;
    this.logo = logo;
    this.summary = summary;
    this.employerName = employerName;
    this.employerId = employerId;
  }
}
