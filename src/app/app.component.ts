import { Component, ViewEncapsulation, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { isPlatformBrowser } from '@angular/common';
import { filter, map, mergeMap } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';
import * as AOS from 'aos';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  constructor(
    private swUpdate: SwUpdate,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private title: Title,
    @Inject(PLATFORM_ID) private platform: any
  ) {}

  ngOnInit(): void {
    AOS.init();
    this.serviceWorkerInit();
    this.metaTagListener();
  }

  public metaTagListener(): void {
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        map(() => this.activatedRoute),
        map((route) => {
          while (route.firstChild) {
            route = route.firstChild;
          }
          return route;
        }),
        filter((route) => route.outlet === 'primary'),
        mergeMap((route) => route.data)
      ).subscribe((event) => {
        this.title.setTitle(event['title']);
        this.onActivate();
      });
  }

  public onActivate(): void {
    if (isPlatformBrowser(this.platform)) {
      window.scroll(0, 0);
    }
  }

  public serviceWorkerInit() {
    this.swUpdate.available.subscribe(() => {
      window.location.reload();
    });
  }
}
