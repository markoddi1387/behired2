import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';

import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';

@Injectable()
export class FileService {
  constructor(
    private angularFireStorage: AngularFireStorage
  ) {}

  public uploadFile(url: string, file: File): AngularFireUploadTask {
    return this.angularFireStorage.upload(url, file);
  }

  public getFile(url: string, type?: string): Observable<string> {
    return this.angularFireStorage.ref(url).getDownloadURL()
      .pipe(
        catchError((error) => throwError(error))
      );
  }

  public deleteFile(url: string): Observable<object> {
    return this.angularFireStorage.ref(url).delete();
  }
}
