import { NgModule } from '@angular/core';

// Services
import { SERVICES } from './index';

@NgModule({
  providers: [
    SERVICES
  ]
})
export class CoreModule { }
