import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable()
export class StorageService {
  private cookieKey = 'BEHIRED_COOKIE';
  private uidKey = 'BEHIRED_UID';
  private hasClientStorage = false;

  constructor() {
    this.hasClientStorage = window.localStorage !== undefined;
  }

  public setCookie(): any {
    localStorage.setItem(this.cookieKey, 'Agreed');
    return { message: 'success' };
  }

  public getCookie(): Observable<any> {
    if (!this.hasClientStorage) {
      return of(null);
    }
    return of(localStorage.getItem(this.cookieKey));
  }

  public setUid(uid: string): Observable<object> {
    localStorage.setItem(this.uidKey, uid);
    return of({ message: 'success' });
  }

  public getUid(): string {
    return localStorage.getItem(this.uidKey);
  }
}
