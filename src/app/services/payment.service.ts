import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { take, flatMap, map, catchError } from 'rxjs/operators';

import { AuthService } from '../services/auth.service';
import { HttpService } from '../services/http.service';

@Injectable()
export class PaymentService extends HttpService {
  constructor(
    public httpClient: HttpClient,
    private authService: AuthService
  ) {
    super(httpClient);
  }

  public pay(token: string, credits: number, featured: number): Observable<any> {
    const url = 'pay/534a5c7146a447349341ac99435a9bdb';

    return this.authService.getUser()
      .pipe(
        take(1),
        flatMap(user => {
          const request = { email: user.email, token, credits, featured };

          return super.post(url, request)
            .pipe(
              take(1),
              map(response => response),
              catchError((error) => {
                throw(error);
              })
            );
        })
      );
  }
}
