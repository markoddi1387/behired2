import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { HttpService } from './http.service';
import { AuthService } from './auth.service';
import { BlogModel, BlogCommentModel } from '../models/blog.model';
import { environment } from '../../environments/environment';

@Injectable()
export class BlogService extends HttpService {

  public key = environment.rssKey;

  constructor(
    public httpClient: HttpClient,
    private authService: AuthService,
    private angularFirestore: AngularFirestore
  ) {
    super(httpClient);
  }

  public getAllBlogs(blogName: string): Observable<object> {
    return super.get(`https://api.rss2json.com/v1/api.json?api_key=${ this.key }&rss_url=https://medium.com/feed/${ blogName }`, {}, true)
      .pipe(
        map(blog => blog['items'])
      );
  }

  public getBlogById(blogName: string, id: string): Observable<object> {
    return super.get(`https://api.rss2json.com/v1/api.json?api_key=${ this.key }&rss_url=https://medium.com/feed/${ blogName }`, {}, true)
      .pipe(
        map(data => {
          return data['items'].filter(blog => {
            blog.guid = blog.guid.split('/')[4];
            return blog.guid === id;
          })[0];
        })
      );
  }

  public subscribeToMailingList(email: string): Observable<object> {
    const data = { email, type: 'blog' };
    return super.post('mailing-blog/534a5caa464427899341a77rfd6tiazbx', data);
  }

  public getComments(id: string): Observable<Array<BlogCommentModel>> {
    return this.angularFirestore.collection('blogComments', ref => ref.where('blogId', '==', id)).snapshotChanges()
      .pipe(
        map(actions => {
          return actions.map(a => {
            const data = a.payload.doc.data() as BlogCommentModel;
            data.id = a.payload.doc.id;
            return data;
          });
        })
      );
  }

  public addComment(currentBlog: BlogModel, comment: string): Observable<any> {
    const owner = this.authService.getUserUid();
    const request = {
      owner,
      comment,
      blogId: currentBlog.guid,
      dateDisplay: moment().format('DD-MM-YYYY HH:mm'),
      date: new Date(),
      vote: 0
    };
    return of(this.angularFirestore.collection('blogComments').add(request));
  }
}
