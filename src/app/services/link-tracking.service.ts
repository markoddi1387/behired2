import { AngularFireDatabase } from '@angular/fire/database';
import { Injectable } from '@angular/core';
import { Observable, from, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { LinkModel } from '../models/link.model';
import { JobModel } from '../models/job.model';
import { FirebaseService } from '../services/firebase.service';

@Injectable()
export class LinkTrackingService extends FirebaseService {
  constructor(
    public angularFireDatabase: AngularFireDatabase
  ) {
    super(angularFireDatabase);
  }

  public setLink(job: JobModel): Subscription {
    return this.getLink(job)
      .pipe(
        take(1)
      ).subscribe((link) => {
        return (link === null) ? this.addLink(job) : this.updateLink(link);
      });
  }

  public getLink(job: JobModel): Observable<void> {
    const ref = this.angularFireDatabase.object(`link-tracking/${ job.id }`);
    return from(super.getObject(ref));
  }

  public updateLink(link: any): Observable<void> {
    link.count++;
    return from(super.update(link, `link-tracking/${ link.id }`));
  }

  public addLink(job: JobModel): Observable<void> {
    const url = job.url ? job.url : `https://behired.co.uk/job/${ job.id }`;
    const linkModel = new LinkModel(job.id, job.name, job.jobType, url);
    return from(super.set(linkModel, `link-tracking/${ job.id }`));
  }
}
