import { map, take, mergeMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpService } from './http.service';
import { UserService } from './user.service';

@Injectable()
export class EmailService extends HttpService {
  constructor(
    public httpClient: HttpClient,
    private userService: UserService
  ) {
    super(httpClient);
  }

  public emailInvoiceRequest(email: string, name: string): any {
    const subject = 'Invoice Payment Enquiry';
    const sendTo = 'contact@behired.co.uk';
    const body =  `<p>From email: ${ email } and company: ${ name } </p><br><p>Enquiry: I would like to pay by invoice, please email me back.</p>`;
    const data = { email: sendTo, body, subject };
    return super.post('email', data);
  }

  public emailContact(name: string, email: string, enquiry: string): Observable<any> {
    const subject = 'Enquiry';
    const sendTo = 'markoddi1387@gmail.com';
    const body =  `<p>Name: ${ name }, Email: ${ email }</p><br><p>Enquiry: ${ enquiry }</p>`;
    const data = { email: sendTo, body, subject };

    return super.post('email', data);
  }

  public emailEmployerJobCreated(jobName: string): Observable<any> {
    const url = 'https://behired.co.uk/employer/jobs';
    const subject = 'Job Created';
    const body =  `
      <p>Job named ${ jobName } has successfully been created and is now active</p>
      <p><a href="${ url }">View Jobs</a></p>`;
    let email;
    let name;

    return this.userService.getEmployerProfile()
      .pipe (
        take(1),
        mergeMap((profile) => {
          email = profile.email;
          name = profile.contactName;
          return super.post('email', { email, body, subject });
        })
      );
  }

  public emailPasswordReset(): Observable<any> {
    const url = 'https://behired.co.uk/signin';
    const subject = 'Password Reset';
    const body =  `
      <p>Your password has successfully been reset</p>
      <p><a href="${ url }">Sign In</a></p>`;
    let email;
    let name;

    return this.userService.getEmployerProfile()
      .pipe (
        take(1),
        mergeMap((profile) => {
          email = profile.email;
          name = profile.contactName;
          return super.post('email', { email, body, subject });
        })
      );
  }

  public emailEmployerJobUpdated(jobName: string): Observable<any> {
    const subject = 'Job Updated';
    const body =  `<p>Job named ${ jobName } has successfully been updated and is now active</p>`;
    let email;

    return this.userService.getEmployerProfile()
      .pipe (
        take(1),
        mergeMap(profile => {
          email = profile.email;
          return super.post('email', { email, body, subject });
        })
      );
  }

  public emailEmployerJobAppliedfor(name: string, email: string, jobName: string, url: string): Observable<any> {
    const subject = 'Applicant Applied';
    const body =  `<p>${ name } has applied for job named ${ jobName }</p><br>
      <p><a href="${ url }">View Applicants</a></p>`;
    const data = { email, body, subject };
    return super.post('email', data);
  }

  public emailApplicantJobAppliedfor(name: string, email: string, jobName: string, url: string): Observable<any> {
    const subject = 'Successful Application';
    const body =  `<p>You've successfully applied for a job named ${ jobName }</p><br>
      <p><a href="${ url }">View Job</a></p>`;
    const data = { email, body, subject };
    return super.post('email', data);
  }

  public emailWelcome(email: string): Observable<any> {
    const subject = 'Welcome to Behired';
    const body =  `
      <p>Welcome ${ email } to Behired</p>
      <p>Its great to have you on board and we're looking forward to working with you!</p>
    `;
    const data = { email, body, subject };
    return super.post('email', data);
  }

  public emailCancelledJob(email: string, jobName: string): Observable<any> {
    const url = 'https://behired.co.uk/employer/jobs';
    const subject = 'Job Cancelled';
    const body = `<p>You've successfully cancelled your job called: ${ jobName }</p>
      <p>This means you won't receive any applications or emails regarding this job.</p><br>
      <p><a href="${ url }">View Jobs</a></p>`;
    const data = { email, body, subject };
    return super.post('email', data);
  }
}
