import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase } from '@angular/fire/database';
import { Injectable } from '@angular/core';
import { Observable, of, zip, concat, forkJoin } from 'rxjs';

import { ApplicantModel } from '../models/applicant.model';
import { FirebaseService } from '../services/firebase.service';
import { JobModel } from '../models/job.model';
import { JobService } from '../services/job.service';
import { UserService } from '../services/user.service';
import { EmailService } from '../services/email.service';

@Injectable()
export class ApplicationService extends FirebaseService {
  constructor(
    private jobService: JobService,
    private emailService: EmailService,
    private userService: UserService,
    public angularFireDatabase: AngularFireDatabase,
    public firebaseAuth: AngularFireAuth
  ) {
    super(angularFireDatabase);
  }

  public getAllApplicants(): Observable<Array<any>> {
    return of([]);
  }

  public getApplicantByFilter(type: string): Observable<Array<any>> {
    return of([]);
  }

  public addApplicantToJob(job: JobModel, applicant: ApplicantModel): Observable<boolean> {
    const application = { key: applicant.applicantId, name: `${ applicant.firstName }
                        ${ applicant.lastName }`, address: applicant.address };

    if (!job.listOfApplications) {
      job.listOfApplications = [ application ];
    } else {
      job.listOfApplications = [ ...job.listOfApplications, application ];
    }
    return of(true);
  }

  public addJobToApplicant(job: JobModel, applicant: ApplicantModel): Observable<boolean> {
    const jobObj = { key: job.key, name: job.name, description: job.description, pay: job.pay, address: job.address };

    if (!applicant.listOfJobApplications) {
      applicant.listOfJobApplications = [ jobObj ];
    } else {
      applicant.listOfJobApplications = [ ...applicant.listOfJobApplications, jobObj ];
    }
    return of(true);
  }

  public applyEmails(job: JobModel, applicant: ApplicantModel): Observable<any> {
    const applicantUrl = `https://behired.co.uk/job/${ job.id }`;
    const employmentUrl = 'https://behired.co.uk/employer/applications/${ job.id }';

    return forkJoin([
      this.emailService.emailApplicantJobAppliedfor(applicant.firstName, applicant.email, job.name, applicantUrl),
      this.emailService.emailEmployerJobAppliedfor(`${ applicant.firstName } ${ applicant.lastName }`,
                                                    job.employerEmail, job.name, employmentUrl)
    ]);
  }

  public applyForJob(job: JobModel, applicant: ApplicantModel): Observable<any> {
    return zip(
      this.addApplicantToJob(job, applicant),
      this.addJobToApplicant(job, applicant),
      this.userService.updateApplicantProfile(applicant),
      this.jobService.updateJobApplicationList(job, job.key)
    );
  }

  public isApplicantApplicable(job: JobModel, applicant: ApplicantModel): Observable<object> {
    let isApplicable = true;
    let message = '';

    if (applicant !== null) {

      if (job.listOfApplications && this.hasApplicantAlreadyApplied(job, applicant)) {
        isApplicable = false;
        message = 'You have already applied for this job';
      }

      if (!applicant.address && !applicant.firstName && !applicant.lastName) {
        isApplicable = false;
        message = 'Please complete your profile to apply';
      }

    } else {
      isApplicable = false;
      message = 'Only applicant accounts can apply for jobs';
    }

    const verifiedApplicant = { isApplicable, message };
    return of({ verifiedApplicant, applicant });
  }

  public hasApplicantAlreadyApplied(job: JobModel, applicant: ApplicantModel): boolean {
    let hasApplied = false;

    job.listOfApplications.forEach(listedApplicant => {
      hasApplied = (listedApplicant.key === applicant.applicantId);
    });

    return hasApplied;
  }
}
