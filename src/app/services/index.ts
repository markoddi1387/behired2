import { ApplicationService } from './application.service';
import { AuthService } from './auth.service';
import { EmailService } from './email.service';
import { FileService } from './file.service';
import { FirebaseService } from './firebase.service';
import { GoogleApiService } from './google-api.service';
import { HttpService } from './http.service';
import { UtilityService } from './utility.service';
import { JobService } from './job.service';
import { NotificationService } from '../services/notification.service';
import { PaymentService } from './payment.service';
import { RouterService } from './router.service';
import { StorageService } from './storage.service';
import { UserService } from './user.service';
import { TransactionService } from './transaction.service';
import { SavedJobService } from './saved-job.service';
import { LinkTrackingService } from './link-tracking.service';
import { BlogService } from './blog.service';
import { SearchLookupService } from './search-lookup.service';
import { CompetitionService } from './competition.service';
import { ReviewService } from './review.service';
import { ChatService } from './chat.service';
import { ForumService } from './forum.service';

export const SERVICES = [
  ChatService,
  ReviewService,
  ApplicationService,
  ForumService,
  AuthService,
  EmailService,
  LinkTrackingService,
  BlogService,
  CompetitionService,
  SavedJobService,
  FileService,
  FirebaseService,
  SearchLookupService,
  GoogleApiService,
  HttpService,
  UtilityService,
  JobService,
  NotificationService,
  PaymentService,
  RouterService,
  StorageService,
  UserService,
  TransactionService
];
