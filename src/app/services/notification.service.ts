import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

import { NotificationModel } from '../models/notification.model';

@Injectable()
export class NotificationService {
  private notifications: Subject<NotificationModel> = new Subject();
  public notifications$ = this.notifications.asObservable();
  private profileStatus: BehaviorSubject<object> = new BehaviorSubject({});
  public profileStatus$ = this.profileStatus.asObservable();
  private creditUpdate: Subject<object> = new Subject();
  public creditUpdate$ = this.creditUpdate.asObservable();

  public updateCreditStatus(isCreditChanged: boolean): any {
    this.creditUpdate.next({ isCreditChanged });
    return;
  }

  public updateNotification(notificationModel: NotificationModel): any {
    this.notifications.next(notificationModel);
    return;
  }

  public updateProfileStatus(status: boolean): any {
    this.profileStatus.next({ status });
    return;
  }
}
