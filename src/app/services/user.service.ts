import { AngularFireDatabase } from '@angular/fire/database';
import { Injectable } from '@angular/core';
import { Observable, from, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { ApplicantModel } from '../models/applicant.model';
import { EmployerModel } from '../models/employer.model';
import { FirebaseService } from '../services/firebase.service';
import { StorageService } from '../services/storage.service';
import { NotificationService } from '../services/notification.service';

@Injectable()
export class UserService extends FirebaseService {
  public applicantBaseUrl = 'applicants';
  public employerBaseUrl = 'employers';

  constructor(
    public angularFireDatabase: AngularFireDatabase,
    public storageService: StorageService,
    public notificationService: NotificationService
  ) {
    super(angularFireDatabase);
  }

  public getApplicantProfile(): Observable<any> {
    const ref = this.angularFireDatabase.object(`${ this.applicantBaseUrl }/${ this.storageService.getUid() }`);
    return from(super.getObject(ref));
  }

  public getApplicantProfileById(applicantId: string): Observable<any> {
    const ref = this.angularFireDatabase.object(`${ this.applicantBaseUrl }/${ applicantId }`);
    return from(super.getObject(ref));
  }

  public addApplicantProfile(profile: any): Observable<void> {
    profile.applicantId = this.storageService.getUid();
    return from(super.set(profile, `${ this.applicantBaseUrl }/${ profile.applicantId }`));
  }

  public updateApplicantProfile(profile: ApplicantModel): Observable<void> {
    return from(
      super.update(profile, `${ this.applicantBaseUrl }/${ profile.applicantId }`).then(() => {
        this.notificationService.updateProfileStatus(true);
      })
    );
  }

  public getEmployerProfile(): Observable<any> {
    const ref = this.angularFireDatabase.object(`${ this.employerBaseUrl }/${ this.storageService.getUid() }`);
    return from(super.getObject(ref));
  }

  public addEmployerProfile(profile: EmployerModel): Observable<void> {
    profile.employerId = this.storageService.getUid();
    return from(super.set(profile, `${ this.employerBaseUrl }/${ profile.employerId }`));
  }

  public updateEmployerProfile(profile: EmployerModel): Observable<void> {
    return from(super.update(profile, `${ this.employerBaseUrl }/${ this.storageService.getUid() }`))
      .pipe(
        map(() => this.notificationService.updateProfileStatus(true))
      );
  }

  public deleteApplicant(id: string): Observable<void> {
    return from(super.delete(`${ this.applicantBaseUrl }/${ id }`));
  }

  public deleteEmployer(id: string): Observable<void> {
    return from(super.delete(`${ this.employerBaseUrl }/${ id }`));
  }
}
