import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

import { ReviewModel } from '../models/review.model';
import { AuthService } from './auth.service';

@Injectable()
export class ReviewService {

  constructor(
    private angularFirestore: AngularFirestore,
    private authService: AuthService
  ) {}

  public postReview(review: ReviewModel): Observable<any> {
    const userUid = this.authService.getUserUid();
    review.ownerId = userUid;
    return of(this.angularFirestore.collection('reviews').add(review));
  }

  public getReviewsByType(type: string): Observable<any> {
    return this.angularFirestore.collection<ReviewModel>('reviews', ref => ref.where('type', '==', type)).snapshotChanges()
      .pipe(
        map(actions => {
          return actions.map(a => {
            const data = a.payload.doc.data() as ReviewModel;
            const id = a.payload.doc.id;
            return { id, ...data };
          });
        })
      );
  }

  public getReviewsById(id: string): Observable<any> {
    return this.angularFirestore.collection('reviews').doc(id).valueChanges();
  }

  public updateReviewById(id: string, review: ReviewModel): Observable<any> {
    return of(this.angularFirestore.collection('reviews').doc(id).update(review));
  }
}
