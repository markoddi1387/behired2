import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { HttpService } from './http.service';

declare let ga: any;

@Injectable()
export class GoogleApiService extends HttpService  {
  constructor(
    public httpClient: HttpClient
  ) {
    super(httpClient);
  }

  public getAddress(placeId: string): Observable<object> {
    const url = `${ environment.googleApiMap }?place_id=${ placeId }&key=${ environment.googleApiKey }`;
    return super.get(url, {}, true)
     .pipe(
        map(data => {
          const address = data['results'][0].formatted_address;
          const location = data['results'][0].geometry.location;
          return { lat: location.lat, lng: location.lng, address, placeId };
        })
     );
  }

  public getCoordinates(postCode: string): Observable<object> {
    const url = `${ environment.googleApiMap }?address=${ postCode }&key=${ environment.googleApiKey }`;
    return super.get(url, {}, true)
     .pipe(
        map(data => {
          const address = data['results'][0].formatted_address;
          const location = data['results'][0].geometry.location;
          return { lat: location.lat, lng: location.lng, address };
        })
     );
  }

  public distanceBetweenMarkers(lat1: any, lon1: any, lat2: any, lon2: any, unit = 'K'): any {
    const radlat1 = Math.PI * lat1 / 180;
    const radlat2 = Math.PI * lat2 / 180;
    const theta = lon1 - lon2;
    const radtheta = Math.PI * theta / 180;
    let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist);
    dist = dist * 180 / Math.PI;
    dist = dist * 60 * 1.1515;

    if (unit === 'K') { dist = dist * 1.609344; }
    if (unit === 'N') { dist = dist * 0.8684; }

    return Math.round(dist);
  }

  public clickEvent(category: string, value: string): void {
    ga('send', 'event', category, value);
  }
}
