import { Injectable } from '@angular/core';
import { LOOKUP } from '../constants/search-lookup.constants';

@Injectable()
export class SearchLookupService {
  constructor() {}

  public findSearchTerm(term: string): any {
    term = term.toLowerCase();
    return  LOOKUP[term] ? LOOKUP[term] : term;
  }
}
