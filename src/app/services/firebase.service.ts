import { AngularFireDatabase } from '@angular/fire/database';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable()
export abstract class FirebaseService {
  constructor(
    public db: AngularFireDatabase
  ) {}

  protected getList(query: any): any {
    return query.snapshotChanges()
      .pipe(
        map(changes => {
          return changes['map'](item => ({ key: item.payload.key, ...item.payload.val() }));
        })
      );
  }

  protected getObject(query: any): Promise<void> {
    return query.snapshotChanges().pipe(map(action => action['payload'].val()));
  }

  protected add(item: object, url: string): any {
    return this.db.list(url).push(item);
  }

  protected update(item: object, url: string): Promise<void> {
    return this.db.object(url).update(item);
  }

  protected set(item: object, url: string): Promise<void> {
    return this.db.object(url).set(item);
  }

  protected delete(url: string): Promise<void> {
    return this.db.object(url).remove();
  }
}
