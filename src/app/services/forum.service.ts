import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

import { ForumModel, ForumCommentModel } from '../models/forum.model';
import { AuthService } from './auth.service';

@Injectable()
export class ForumService {

  constructor(
    public angularFirestore: AngularFirestore,
    private authService: AuthService
  ) {}

  public getForums(): Observable<any> {
    return this.angularFirestore.collection('forum').snapshotChanges()
      .pipe(
        map(actions => {
          return actions.map(a => {
            const data = a.payload.doc.data() as ForumModel;
            const id = a.payload.doc.id;
            return { id, ...data };
          });
        })
      );
  }

  public getForumComments(id: string): Observable<Array<ForumCommentModel>> {
    return this.angularFirestore.collection('forumComments', ref => ref.where('forumId', '==', id)).snapshotChanges()
      .pipe(
        map(actions => {
          return actions.map(a => {
            const data = a.payload.doc.data() as ForumCommentModel;
            data.id = a.payload.doc.id;
            return data;
          });
        })
      );
  }

  public addComment(currentForum: ForumModel, comment: string): Observable<any> {
    const owner = this.authService.getUserUid();
    const request = {
      owner,
      comment,
      forumId: currentForum.id,
      dateDisplay: moment().format('DD-MM-YYYY HH:mm'),
      date: new Date(),
      vote: 0
    };
    return of(this.angularFirestore.collection('forumComments').add(request));
  }

  public updateComment(forumComment: ForumCommentModel): Observable<any> {
    return of(this.angularFirestore.collection('forumComments').doc(forumComment.id).update(forumComment));
  }
}
