import * as firebase from 'firebase/app';
import Geohash from 'latlon-geohash';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Observable, from, of } from 'rxjs';
import { GeoFirestore } from 'geofirestore';
import { map, take } from 'rxjs/operators';

import { FirebaseService } from '../services/firebase.service';
import { JobModel } from '../models/job.model';
import { StorageService } from '../services/storage.service';
import { UserService } from '../services/user.service';
import { UtilityService } from '../services/utility.service';

@Injectable()
export class JobService extends FirebaseService {
  public geo: any;
  public geofirestore: GeoFirestore;

  constructor(
    public angularFireDatabase: AngularFireDatabase,
    private angularFirestore: AngularFirestore,
    public storageService: StorageService,
    private userService: UserService,
    private utilityService: UtilityService
  ) {
    super(angularFireDatabase);

    const firestore = firebase.firestore();
    firestore.settings({ });
    this.geofirestore = new GeoFirestore(firestore);
  }

  public getCount(): any {
    const adzuna = firebase.firestore().collection('jobsCount').doc('adzuna');
    const behired = firebase.firestore().collection('jobsCount').doc('behired');

    return from(Promise.all([adzuna.get(), behired.get()])
      .then((querySnapshot) => {
        return { adzuna: querySnapshot[0].data(), behired: querySnapshot[1].data() };
      })
    );
  }

  public getJobs(queryType: string, selectedFilterTypes: any, geo: any,
                 lastItemBehired: any, lastItemAdzuna: any, isPagination: boolean): Observable<any> {
    if (queryType === 'geo') {
      const behiredGeoCollection = this.geofirestore.collection('behired');
      const adzunaGeoCollection = this.geofirestore.collection('adzuna');

      const behiredQuery = behiredGeoCollection.near(
        { center: new firebase.firestore.GeoPoint(geo.lat, geo.lng), radius: Number(geo.radius) });
      const adzunaQuery = adzunaGeoCollection.near(
        { center: new firebase.firestore.GeoPoint(geo.lat, geo.lng), radius: Number(geo.radius) });

      return from(Promise.all([
        this.utilityService.queryBuilder(behiredQuery, selectedFilterTypes, queryType),
        this.utilityService.queryBuilder(adzunaQuery, selectedFilterTypes, queryType)
      ]).then((data) => {
        const behired = data[0].jobs;
        const adzuna = data[1].jobs;

        return {
          behiredJobs: behired,
          adzunaJobs: adzuna,
          lastItemAdzuna: null,
          lastItemBehired: null
        };
      }));
    } else {
      let behiredQuery = firebase.firestore().collection('behired').orderBy('d.datePosted', 'desc');
      let adzunaQuery = firebase.firestore().collection('adzuna').orderBy('d.datePosted', 'desc');

      // Note: pagination behired
      if (lastItemBehired !== null && isPagination) {
        behiredQuery = behiredQuery.startAfter(lastItemBehired).limit(1000);
      }

      if (lastItemBehired === null && isPagination) {
        behiredQuery = behiredQuery.limit(1);
      }

      if (lastItemBehired !== null && !isPagination) {
        behiredQuery = behiredQuery.limit(1000);
      }

      // Note: pagination adzuna
      if (lastItemAdzuna !== null && isPagination) {
        adzunaQuery = adzunaQuery.startAfter(lastItemAdzuna).limit(20);
      } else {
        adzunaQuery = adzunaQuery.limit(20);
      }

      return from(Promise.all([
        this.utilityService.queryBuilder(behiredQuery, selectedFilterTypes, queryType),
        this.utilityService.queryBuilder(adzunaQuery, selectedFilterTypes, queryType)
      ]).then((data) => {
        const behired = data[0];
        const adzuna = data[1];

        if (isPagination) {
          behired.jobs = [];
        }

        return {
          behiredJobs: behired.jobs,
          adzunaJobs: adzuna.jobs,
          lastItemAdzuna: adzuna.lastItem,
          lastItemBehired: behired.lastItem
        };
      }));
    }
  }

  public getApplicantsJobs(): Observable<any> {
    const uid = this.storageService.getUid();
    const query = this.angularFireDatabase.list('applications', ref => ref.orderByChild('applicantId').equalTo(uid));
    return from(super.getList(query));
  }

  public getExternalJobById(jobId: string): Observable<any> {
    return this.angularFirestore.collection('behired').doc(jobId).valueChanges();
  }

  public getJobById(jobId: string): Observable<any> {
    const ref = firebase.firestore().collection('behired').doc(jobId);

    return from(ref.get().then(querySnapshot => {
      return this.utilityService.mapObject(querySnapshot);
    }));
  }

  public getAllBehiredJobs(): Observable<any> {
    return this.angularFirestore.collection('behired').snapshotChanges();
  }

  public getAllActiveBehiredJobs(): Observable<any> {
    const ref = firebase.firestore().collection('behired').where('d.isActive', '==', true);
    return from(ref.get());
  }

  public getAllActiveBehiredJobsByLimit(limitTo = 6): any {
    const ref = firebase.firestore().collection('behired').where('d.isActive', '==', true).limit(limitTo);

    return from(ref.get().then(querySnapshot => {
      return this.utilityService.mapArray(querySnapshot).jobs;
    }));
  }

  public cancelJob(jobId: string): Observable<any> {
    const ref = firebase.firestore().collection('behired').doc(jobId);
    return from(ref.update({ 'd.isActive': false, 'd.cancelCounter': 1 }));
  }

  public deleteJob(jobId: string): Observable<any> {
    const ref = firebase.firestore().collection('behired').doc(jobId);
    return from(ref.delete());
  }

  public getEmployersJobs(): Observable<any> {
    const uid = this.storageService.getUid();
    const ref = firebase.firestore().collection('behired').where('d.employerId', '==', uid);

    return from(ref.get().then(querySnapshot => {
      return this.utilityService.mapAllStatusJobsArray(querySnapshot).jobs;
    }));
  }

  public createJob(job: JobModel, geometry: object): Observable<any> {
    job.isActive = true;
    job.address.latitude = geometry['lat'];
    job.address.longitude = geometry['lng'];
    job.address.placeId = geometry['placeId'];
    job.address.addressLine1 = geometry['address'];
    job.employerId = this.storageService.getUid();
    job.brand = 'behired';

    return this.userService.getEmployerProfile()
      .pipe(
        map(profile => {
          job.employerEmail = profile.email;
          job.employerName = profile.companyName;
          job.logo = (profile.logo) ? profile.logo : null;

          const geoHash = Geohash.encode(job.address.latitude, job.address.longitude, [ 9 ]);
          const ref = firebase.firestore().collection('behired');

          const jobObj = {
            d: job,
            g: geoHash,
            l: new firebase.firestore.GeoPoint(parseFloat(job.address.latitude), parseFloat(job.address.longitude))
          };

          return from(ref.add(jobObj));
        })
      );
  }

  public updateJob(job: JobModel, jobId: string, coords?: any): Observable<any> {
    job.dateLastUpdated = new Date();

    if (coords) {
      job.address.latitude = coords['lat'];
      job.address.longitude = coords['lng'];
    }

    return this.userService.getEmployerProfile()
      .pipe(
        map(profile => {
          job.employerEmail = profile.email;
          job.employerName = profile.companyName;
          job.employerId = profile.employerId;
          job.logo = (profile.logo) ? profile.logo : null;

          const geoHash = Geohash.encode(job.address.latitude, job.address.longitude, [ 9 ]);
          const ref = firebase.firestore().collection('behired').doc(jobId);

          const jobObj = {
            d: job,
            g: geoHash,
            l: new firebase.firestore.GeoPoint(parseFloat(job.address.latitude), parseFloat(job.address.longitude))
          };

          from(ref.update(jobObj));
        })
      );
  }

  public updateJobApplicationList(job: JobModel, jobId: string): Observable<any> {
    const ref = firebase.firestore().collection('behired').doc(jobId);
    return from(from(ref.update({ d: job })));
  }

  public deleteByEmployerId(): Observable<any> {
    return of(this.getEmployersJobs()
      .pipe(
        take(1)
      ).subscribe((jobs) => {
        jobs.forEach(job => {
          this.deleteJob(job.key);
        });
      }));
  }
}
