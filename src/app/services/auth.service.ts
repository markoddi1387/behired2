import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import { take, map } from 'rxjs/operators';

import { StorageService } from './storage.service';

@Injectable()
export class AuthService {
  public email: string;

  constructor(
    private angularFireAuth: AngularFireAuth,
    private storageService: StorageService
  ) {}

  get getCurrentEmail(): string {
    return this.email;
  }

  set setCurrentEmail(email: string) {
    this.email = email;
  }

  public login(email: string, password: string): Observable<firebase.auth.UserCredential> {
    const login = this.angularFireAuth.signInWithEmailAndPassword(email, password);
    return from(login.then(response => response).catch(e => e));
  }

  public signInAnonymously(): Observable<firebase.auth.UserCredential> {
    const login = this.angularFireAuth.signInAnonymously();
    return from(login.then(response => response).catch(e => e));
  }

  public signup(email: string, password: string, type: string): Observable<firebase.auth.UserCredential> {
    const signup = this.angularFireAuth.createUserWithEmailAndPassword(email, password);
    return from(signup.then(response => response).catch(e => e));
  }

  public logout(): Observable<any> {
    this.storageService.setUid('');
    return from(this.angularFireAuth.signOut());
  }

  public resetPassword(code: string, password: string): Observable<any> {
    const reset = this.angularFireAuth.confirmPasswordReset(code, password);
    return from(reset.then(response => response).catch(e => e));
  }

  public changePassword(password: string): Observable<any> {
    return from(this.angularFireAuth.currentUser.then(currentUser => {
      currentUser.updatePassword(password);
    }));
  }

  public sendForgotPassword(email: string): Observable<any> {
    const forgot = this.angularFireAuth.sendPasswordResetEmail(email);
    return from(forgot.then(response => response).catch(e => e));
  }

  public sendVerifyEmail(): Observable<any> {
    return from(this.angularFireAuth.currentUser.then(currentUser => {
      currentUser.sendEmailVerification();
    }));
  }

  public emailVerified(code: string): Observable<any> {
    const verified = this.angularFireAuth.applyActionCode(code);
    return from(verified.then(response => response).catch(e => e));
  }

  public getUser(): Observable<firebase.User> {
    return this.angularFireAuth.authState;
  }

  public getUserUid(): string {
    let uid;
    this.angularFireAuth.currentUser.then(currentUser => {
      uid = currentUser.uid;
    });
    return uid;
  }

  public getCurrentUser(): any {
    let user;
    this.angularFireAuth.currentUser.then(currentUser => {
      user = currentUser;
    });
    return user;
  }

  public deleteUser(): Observable<Promise<void>> {
    return from(this.angularFireAuth.authState
      .pipe(
        take(1),
        map((auth) => auth.delete())
      ));
  }
}
