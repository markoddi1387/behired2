import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';

@Injectable()
export abstract class HttpService {
  protected baseUrl = environment.baseUrl;
  protected headers: HttpHeaders;

  constructor(public httpClient: HttpClient) {
    this.setHeader();
  }

  protected setHeader(): void {
    this.headers = new HttpHeaders({
      authorization: 'mango',
    });
  }

  protected get<T>(path: string, options: any = {}, noBaseUrl = false): Observable<T> {
    const url: string  = this.createUrlString(path, noBaseUrl);
    const params: HttpParams = this.getHttpParams(options.queryString);
    return this.httpClient.get<T>(url, { params });
  }

  protected post<T>(path: string, data = {}, noBaseUrl = false): Observable<T> {
    this.setHeader();
    const url: string = this.createUrlString(path, noBaseUrl);
    const options = { headers: this.headers };
    return this.httpClient.post<T>(url, data, options);
  }

  protected createUrlString(resourcePath: string, noBaseUrl: boolean): string {
    return (noBaseUrl) ? `${ resourcePath }` : `${ this.baseUrl }${ resourcePath }`;
  }

  protected getHttpParams(params: any): HttpParams {
    let httpParams: HttpParams = new HttpParams();

    if (params) {
      for (const prop in params) {
        if (params.hasOwnProperty(prop)) {
            const parameterValue: string = params[prop].toString();
            httpParams = httpParams.append(prop, parameterValue);
        }
      }
    }
    return httpParams;
  }
}
