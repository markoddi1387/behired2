import { Injectable } from '@angular/core';

@Injectable()
export class UtilityService {
  public mapAllStatusJobsArray(querySnapshot: any): any {
    let jobs = [];

    querySnapshot.forEach((doc) => {
      const newJob = doc.data().d || doc.data();
      newJob['id'] = doc.id;

      jobs = [ ...jobs, newJob ];
    });

    const lastItem = querySnapshot.docs[querySnapshot.size - 1];
    return { jobs, lastItem };
  }

  public mapArray(querySnapshot: any): any {
    let jobs = [];

    querySnapshot.forEach((doc) => {
      const newJob = doc.data().d || doc.data();
      newJob['id'] = doc.id;

      // Adzuna isActive are all undefined
      if (newJob.isActive || newJob.isActive === undefined) {
        jobs = [ ...jobs, newJob ];
      }
    });

    const lastItem = querySnapshot.docs[querySnapshot.size - 1];
    return { jobs, lastItem };
  }

  public mapObject(querySnapshot: any): any {
    let job = null;
    const data = querySnapshot.data();

    if (data) {
      job = data.d;
      job['id'] = querySnapshot.id;
    }

    return job;
  }

  public queryBuilder(query: any, selectedFilterTypes: any, queryType: string): any {
    if (selectedFilterTypes.sectors !== '') {
      const field = queryType === 'geo' ? 'sector' : 'd.sector';
      query = query.where(field, '==', selectedFilterTypes.sectors);
    }

    if (selectedFilterTypes.payTypes !== '') {
      const field = queryType === 'geo' ? 'payType' : 'd.payType';
      query = query.where(field, '==', selectedFilterTypes.payTypes);
    }

    if (selectedFilterTypes.brands !== '') {
      const field = queryType === 'geo' ? 'brand' : 'd.brand';
      query = query.where(field, '==', selectedFilterTypes.brands);
    }

    if (selectedFilterTypes.term !== '') {
      const field = queryType === 'geo' ? 'tags' : 'd.tags';
      query = query.where(field, 'array-contains', selectedFilterTypes.term);
    }

    return query.get().then((querySnapshot) => {
      return this.mapArray(querySnapshot);
    });
  }
}
