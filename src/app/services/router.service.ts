import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { map, take } from 'rxjs/operators';

import { UserService } from './user.service';

@Injectable()
export class RouterService {
  public isApplicant: boolean;

  constructor(
    private router: Router,
    private userService: UserService
  ) {}

  public redirectProfile(): void {
    this.userService.getApplicantProfile()
      .pipe(
        take(1),
        map((applicant) => this.isApplicant = (applicant !== null))
      ).subscribe(() => {
        this.isApplicant ? this.router.navigate(['/applicant/profile']) : this.router.navigate(['/employer/profile']);
      });
  }

  public redirectJobs(): void {
    this.userService.getApplicantProfile()
      .pipe(
        take(1),
        map((applicant) => this.isApplicant = (applicant !== null))
      ).subscribe(() => {
        this.isApplicant ? this.router.navigate(['/applicant/jobs']) : this.router.navigate(['/employer/jobs']);
      });
  }
}
