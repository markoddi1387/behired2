import { AngularFireDatabase } from '@angular/fire/database';
import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';

import { FirebaseService } from '../services/firebase.service';
import { CompetitionModel } from '../models/competition.model';

@Injectable()
export class CompetitionService extends FirebaseService {
  constructor(
    public angularFireDatabase: AngularFireDatabase
  ) {
    super(angularFireDatabase);
  }

  public apply(email: string, type: string): Observable<any> {
    const application = new CompetitionModel(email, type);
    return from(super.set(application, 'competition'));
  }
}
