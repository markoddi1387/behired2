import { AngularFireDatabase } from '@angular/fire/database';
import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';

import { FirebaseService } from '../services/firebase.service';
import { StorageService } from '../services/storage.service';
import { NotificationService } from '../services/notification.service';
import { CreditModel } from '../models/credit.model';

@Injectable()
export class TransactionService extends FirebaseService {
  private transactionBaseUrl = 'credits';

  constructor(
    public angularFireDatabase: AngularFireDatabase,
    private storageService: StorageService,
    private notificationService: NotificationService
  ) {
    super(angularFireDatabase);
  }

  public updateCredits(creditTotal: number, featuredTotal: number, token: string): Observable<any> {
    const credit = new CreditModel(this.storageService.getUid(), featuredTotal, creditTotal, token);

    return from(super.update(credit, `${ this.transactionBaseUrl }/${ credit.employerUid }`))
      .pipe(
        map(() => this.notificationService.updateCreditStatus(true))
      );
  }

  public createCredit(creditTotal: number, featuredTotal: number): Observable<any> {
    const credit = new CreditModel(this.storageService.getUid(), featuredTotal, creditTotal);
    return from(super.set(credit, `${ this.transactionBaseUrl }/${ credit.employerUid }`))
      .pipe(
        map(() => this.notificationService.updateCreditStatus(true))
      );
  }

  public getCredits(): Observable<any> {
    const employerUid = this.storageService.getUid();
    const ref = this.angularFireDatabase.object(`${ this.transactionBaseUrl }/${ employerUid }`);
    return from(super.getObject(ref));
  }

  public deleteCredit(): Observable<any> {
    const uid = this.storageService.getUid();
    return from(super.delete(`${ this.transactionBaseUrl }/${ uid }`));
  }
}
