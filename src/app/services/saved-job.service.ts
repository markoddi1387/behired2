import { AngularFireDatabase } from '@angular/fire/database';
import { Injectable } from '@angular/core';
import { Observable, from, of } from 'rxjs';

import { SavedJobModel } from '../models/saved-job.model';
import { JobModel } from './../models/job.model';
import { FirebaseService } from '../services/firebase.service';
import { StorageService } from '../services/storage.service';

@Injectable()
export class SavedJobService extends FirebaseService {

  constructor(
    public angularFireDatabase: AngularFireDatabase,
    public storageService: StorageService
  ) {
    super(angularFireDatabase);
  }

  public getSavedJob(): Observable<any> {
    const applicantUid = this.storageService.getUid();

    if (applicantUid === null || applicantUid === '') {
      return of(null);
    }

    const ref = this.angularFireDatabase.object(`saved-jobs/${ applicantUid }`);
    return from(super.getObject(ref));
  }

  public getAllSavedJobs(): Observable<any> {
    const applicantUid = this.storageService.getUid();

    if (applicantUid === null || applicantUid === '') {
      return of([]);
    }

    const query = this.angularFireDatabase.list(`saved-jobs/${ applicantUid }`);
    return from(super.getList(query));
  }

  public addSavedJob(savedJobKey: string, job: JobModel): Observable<void> {
    const isLogoAvailable = job.logo ? job.logo : null;
    const savedJob = new SavedJobModel(savedJobKey, savedJobKey, job.name, job.description, job.address, job.pay,
                                       job.payType, isLogoAvailable, job.summary, job.employerName, job.employerId);
    const applicantUid = this.storageService.getUid();

    if (applicantUid === null || applicantUid === '') {
      return of(null);
    }

    return from(super.set(savedJob, `saved-jobs/${ applicantUid }/${ savedJobKey }`));
  }

  public deleteSavedJob(savedJobKey: string): Observable<void> {
    const applicantUid = this.storageService.getUid();
    const url = `saved-jobs/${ applicantUid }/${ savedJobKey }`;

    if (applicantUid === null || applicantUid === '') {
      return of(null);
    }

    return from(super.delete(url));
  }
}
