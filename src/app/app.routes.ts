import { MetaGuard } from '@ngx-meta/core';

import { Routes } from '@angular/router';

// Layouts:
import { CenterLayout } from './layouts/center/center.layout';
import { HomeLayout } from './layouts/home/home.layout';
import { InfoLayout } from './layouts/info/info.layout';
import { SidebarLayout } from './layouts/sidebar/sidebar.layout';
import { FullWidthLayout } from './layouts/full-width/full-width.layout';
import { CompetitionLayout } from './layouts/competition/competition.layout';
import { CoursesLayout } from './layouts/courses/courses.layout';
import { VolunteerLayout } from './layouts/volunteer/volunteer.layout';

// Components:
import { AccountManagementComponent } from './components/account-management/account-management.component';
import { ContactComponent } from './components/contact/contact.component';
import { CookieComponent } from './components/cookie/cookie.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { HomeComponent } from './components/home/home.component';
import { JobBoardComponent } from './components/job-board/job-board.component';
import { JobBoardItemComponent } from './components/job-board-item/job-board-item.component';
import { SigninComponent } from './components/signin/signin.component';
import { PrivacyComponent } from './components/privacy/privacy.component';
import { SignupComponent } from './components/signup/signup.component';
import { TermsComponent } from './components/terms/terms.component';
import { SettingsComponent } from './components/settings/settings.component';
import { PricingComponent } from './components/pricing/pricing.component';
import { PaymentComponent } from './components/payment/payment.component';
import { AboutComponent } from './components/about/about.component';
import { FaqComponent } from './components/faq/faq.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { WelcomeBackComponent } from './components/welcome-back/welcome-back.component';
import { PasswordResetComponent } from './components/password-reset/password-reset.component';
import { BlogComponent } from './components/blog/blog.component';
import { BlogArticleComponent } from './components/blog-article/blog-article.component';
import { PaymentTermsComponent } from './components/payment-terms/payment-terms.component';
import { CompetitionComponent } from './components/competition/competition.component';
import { BlogVideoComponent } from './components/blog-video/blog-video.component';
import { GroupedJobsComponent } from './components/grouped-jobs/grouped-jobs.component';
import { CoursesComponent } from './components/courses/courses.component';
import { VolunteerComponent } from './components/volunteer/volunteer.component';
import { ForumComponent } from './components/forum/forum.component';

// Guards:
import { AuthGuard } from './guards/auth.guard';

const TITLE = 'Behired |';

export const ROUTES: Routes = [
  {
    path: '',
    pathMatch: 'full',
    canActivateChild: [ MetaGuard ],
    component: HomeLayout,
    children: [
      {
        path: '',
        component: HomeComponent,
        data: {
          id: 'home',
          title: `${ TITLE } Jobs`,
          meta: {
            title: ` Behired Jobs`,
            description: 'Behired - The most efficient way to Behired, sign up today for free and begin your journey with us'
          }
        }
      }
    ]
  },
  {
    path: '',
    canActivateChild: [ MetaGuard ],
    component: FullWidthLayout,
    children: [
      {
        path: 'job-board',
        component: JobBoardComponent,
        data: { title: `${ TITLE } Job Board`,
          meta: {
            title: `${ TITLE } Job Board`,
            description: 'Behired job board'
          }
        }
      },
      {
        path: 'forum',
        component: ForumComponent,
        data: { title: `${ TITLE } Forum`,
          meta: {
            title: `${ TITLE } Forum`,
            description: 'Behired forum'
          }
        }
      }
    ]
  },
  {
    path: '',
    canActivateChild: [ MetaGuard ],
    component: InfoLayout,
    children: [
      {
        path: '404',
        component: NotFoundComponent,
        data: { title: `${ TITLE } 404`,
          meta: {
            title: `${ TITLE } 404`,
            description: `${ TITLE } 404 - Sorry the page you tried cannot be found.`
          }
        }
      },
      {
        path: 'terms',
        component: TermsComponent,
        data: { title: `${ TITLE } Terms & Conditions`,
          meta: {
            title: `${ TITLE } Terms & Conditions`,
            description: 'Behired terms & conditions'
          }
        }
      },
      {
        path: 'about',
        component: AboutComponent,
        data: { title: `${ TITLE } About Us`,
          meta: {
            title: `${ TITLE } About Us`,
            description: 'Behired About Us'
          }
        }
      },
      {
        path: 'blog',
        component: BlogComponent,
        data: { title: `${ TITLE } Blog`,
          meta: {
            title: `${ TITLE } Blog`,
            description: 'Behired Blog'
          }
        }
      },
      {
        path: 'blog-videos/:id',
        component: BlogVideoComponent,
        data: { title: `${ TITLE } Blog Videos`,
          meta: {
            title: `${ TITLE } Blog Videos`,
            description: 'Behired Blog Videos'
          }
        }
      },
      {
        path: 'blog/:id',
        component: BlogArticleComponent,
        data: { title: `${ TITLE } Blog`,
          meta: {
            title: `${ TITLE } Blog`,
            description: 'Behired Blog'
          }
        }
      },
      {
        path: 'faq',
        component: FaqComponent,
        data: { title: `${ TITLE } FAQ`,
          meta: {
            title: `${ TITLE } FAQ`,
            description: 'Behired FAQ'
          }
        }
      },
      {
        path: 'privacy',
        component: PrivacyComponent,
        data: { title: `${ TITLE } Privacy Policy`,
          meta: {
            title: `${ TITLE } Privacy Policy`,
            description: 'Behired privacy policy'
          }
        }
      },
      {
        path: 'payment-terms',
        component: PaymentTermsComponent,
        data: { title: `${ TITLE } Payment Terms`,
          meta: {
            title: `${ TITLE } Payment Terms`,
            description: 'Behired Payment Terms'
          }
        }
      },
      {
        path: 'pricing',
        component: PricingComponent,
        data: { title: `${ TITLE } Pricing`,
          meta: {
            title: `${ TITLE } Pricing`,
            description: 'Behired pricing'
          }
        }
      },
      {
        path: 'cookie',
        component: CookieComponent,
        data: { title: `${ TITLE } Cookie Policy`,
          meta: {
            title: `${ TITLE } Cookie Policy`,
            description: 'Behired cookie policy'
          }
        }
      },
      {
        path: 'jobs',
        component: GroupedJobsComponent,
        data: { title: `${ TITLE } Job Board`,
          meta: {
            title: `${ TITLE } Job Board`,
            description: 'Behired job board'
          }
        }
      },
      {
        path: 'jobs/:employerId',
        component: GroupedJobsComponent,
        data: { title: `${ TITLE } Job Board`,
          meta: {
            title: `${ TITLE } Job Board`,
            description: 'Behired job board'
          }
        }
      },
      {
        path: 'job-board/:term',
        component: JobBoardComponent,
        data: { title: `${ TITLE } Job Board`,
          meta: {
            title: `${ TITLE } Job Board`,
            description: 'Behired job board'
          }
        }
      },
      {
        path: 'job-board/:term/:postCode/:postCodeDisplay/:radius',
        component: JobBoardComponent,
        data: { title: `${ TITLE } Job Board`,
          meta: {
            title: `${ TITLE } Job Board`,
            description: 'Behired job board'
          }
        }
      },
      {
        path: 'job/:id',
        component: JobBoardItemComponent,
        data: { title: `${ TITLE } Job`,
          meta: {
            title: `${ TITLE } Job`,
            description: 'Behired job'
          }
        }
      }
    ]
  },
  {
    path: '',
    canActivateChild: [ MetaGuard ],
    component: CompetitionLayout,
    children: [
      {
        path: 'competition/:type',
        component: CompetitionComponent,
        data: {
          title: `${ TITLE } Prize Draw`,
          meta: {
            title: `${ TITLE } Prize Draw`,
            description: 'Behired prize draw'
          }
        }
      }
    ]
  },
  {
    path: '',
    canActivateChild: [ MetaGuard ],
    component: CoursesLayout,
    children: [
      {
        path: 'courses',
        component: CoursesComponent,
        data: {
          title: `${ TITLE } Courses`,
          meta: {
            title: `${ TITLE } Courses`,
            description: 'Behired find your courses'
          }
        }
      }
    ]
  },
  {
    path: '',
    canActivateChild: [ MetaGuard ],
    component: VolunteerLayout,
    children: [
      {
        path: 'volunteer',
        component: VolunteerComponent,
        data: {
          title: `${ TITLE } Volunteer`,
          meta: {
            title: `${ TITLE } Volunteer`,
            description: 'Behired Volunteer'
          }
        }
      }
    ]
  },
  {
    path: '',
    canActivateChild: [ MetaGuard ],
    component: CenterLayout,
    children: [
      {
        path: 'signin',
        component: SigninComponent,
        data: {
          title: `${ TITLE } Sign In`,
          meta: {
            title: `${ TITLE } Sign In`,
            description: 'Welcome to Behired, sign in and get started!'
          }
        }
      },
      {
        path: 'signin/:type',
        component: SigninComponent,
        data: {
          title: `${ TITLE } Sign In`,
          meta: {
            title: `${ TITLE } Sign In`,
            description: 'Welcome to Behired, sign in and get started!'
          }
        }
      },
      {
        path: 'signup',
        component: SignupComponent,
        data: {
          title: `${ TITLE } Sign Up`,
          meta: {
            title: `${ TITLE } Sign Up`,
            description: 'Behired - join us today!'
          }
        }
      },
      {
        path: 'signup/:recruiter',
        component: SignupComponent,
        data: {
          title: `${ TITLE } Sign Up Recruiter`,
          meta: {
            title: `${ TITLE } Sign Up Recruiter`,
            description: 'Behired - recruit with us'
          }
        }
      },
      {
        path: 'forgot-password',
        component: ForgotPasswordComponent,
        data: {
          title: `${ TITLE } Forgot Password`,
          meta: {
            title: `${ TITLE } Forgot Password`,
            description: 'Behired forgot password'
          }
        }
      },
      {
        path: 'welcome-back/:email',
        component: WelcomeBackComponent,
        data: {
          title: `${ TITLE } Welcome Back`,
          meta: {
            title: `${ TITLE } Welcome Back`,
            description: 'Behired welcome back'
          }
        }
      },
      {
        path: 'contact',
        component: ContactComponent,
        data: {
          title: `${ TITLE } Contact Us`,
          meta: {
            title: `${ TITLE } Contact Us`,
            description: 'Behired contact us'
          }
        }
      },
      {
        path: 'account-management',
        component: AccountManagementComponent,
        data: { title: `${ TITLE } account management` }
      }
    ]
  },
  {
    path: '',
    canActivateChild: [ MetaGuard ],
    component: SidebarLayout,
    canActivate: [ AuthGuard ],
    children: [
      {
        path: 'settings',
        component: SettingsComponent,
        data: { title: `${ TITLE } Settings`,
          meta: {
            title: `${ TITLE } Settings`,
            description: 'Behired settings'
          }
        }
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
        data: { title: `${ TITLE } Dashboard`,
          meta: {
            title: `${ TITLE } Dashboard`,
            description: 'Behired dashboard'
          }
        }
      },
      {
        path: 'password-reset',
        component: PasswordResetComponent,
        data: { title: `${ TITLE } Password Reset`,
          meta: {
            title: `${ TITLE } Password Reset`,
            description: 'Behired password reset'
          }
        }
      },
      {
        path: 'payment',
        component: PaymentComponent,
        data: { title: `${ TITLE } Payment`,
          meta: {
            title: `${ TITLE } Payment`,
            description: 'Behired payments'
          }
        }
      },
      {
        path: 'payment/:id',
        component: PaymentComponent,
        data: { title: `${ TITLE } Payment`,
          meta: {
            title: `${ TITLE } Payment`,
            description: 'Behired payments'
          }
        }
      }
    ]
  },
  {
    path: 'employer',
    loadChildren: () => import('./components/employer/employer.module').then(m => m.EmployerModule)
  },
  {
    path: 'reviews',
    loadChildren: () => import('./components/reviews/reviews.module').then(m => m.ReviewsModule)
  },
  {
    path: 'applicant',
    loadChildren: () => import('./components/applicant/applicant.module').then(m => m.ApplicantModule)
  },
  {
    path: '**', redirectTo: '404'
  }
];
