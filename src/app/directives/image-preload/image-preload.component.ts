import { Directive, Input, HostBinding } from '@angular/core';

@Directive({
  selector: 'img[default]',
  host: {
    '(error)': 'updateUrl()',
    '(load)': 'load()',
    '[src]': 'src'
  }
})
export class ImagePreloadDirective {
  @Input() src: string;
  @Input() default: string;
  @HostBinding('class') className;

  public updateUrl(): void {
    if (!this.className) {
      this.className = 'hideLogo';
    }

    this.src = this.default;
  }

  public load(): void {
    this.className = 'logo';
  }
}
