import { DropZoneDirective } from './drop-zone/drop-zone.component';
import { ImagePreloadDirective } from './image-preload/image-preload.component';

export const DIRECTIVES = [
  DropZoneDirective,
  ImagePreloadDirective
];
