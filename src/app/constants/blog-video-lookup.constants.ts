export const LOOKUP = {
  'factor-5-workout': { title: 'Factor 5 Workout', description: `test`, url: 'https://www.youtube.com/embed/Vhr-u_q2zz0' },
  'great-balls-of-fire-workout': { title: 'Great Balls of Fire workout', description: `test`, url: 'https://www.youtube.com/embed/DpM1evKKT6A' },
  'power-packed-workout': { title: 'Power Packed Workout', description: `test`, url: 'https://www.youtube.com/embed/D9sPqWnWkXE' },
  'total-body-tabata-circuit-workout': { title: 'Total Body Tabata Circuit Workout', description: `test`, url: 'https://www.youtube.com/embed/gs2vTEMSczU' },
  'upstairs-downstairs-workout': { title: 'Upstairs Downstairs Workout', description: `test`, url: 'https://www.youtube.com/embed/ufnkMVtO6DI' },
  'whole-body-dumbell-workout': { title: 'Whole Body Dumbell Workout', description: `test`, url: 'https://www.youtube.com/embed/C1ScwFt7fVE' },
  'cardio-ab-workout': { title: 'Cardio AB Workout', description: `test`, url: 'https://www.youtube.com/embed/uxNaQE4JyE4' },
  'boxing-round-workout': { title: 'Boxing Round Workout', description: `test`, url: 'https://www.youtube.com/embed/XTUGYuZZ90w' },
  'bum-legs-and-tums-workout': { title: 'Bum Legs and Tums Workout', description: `test`, url: 'https://www.youtube.com/embed/TrVnvMsK9_g' }
};
