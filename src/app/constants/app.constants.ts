export const APP = {
  radius: [ 1, 2, 3, 4, 5, 8, 10, 12, 15, 18, 20 ],
  sectors: [ 'Swimming Teacher', 'Lifeguard', 'Catering', 'Personal Trainer', 'Medical', 'Office', 'Spa', 'Yoga', 'Coach', 'Volunteer' ],
  // positions: [ 'Manager', 'Deputy Manager', 'Supervisor', 'Team Lead', 'Assistant', 'Consultant' ],
  locations: [ 'London', 'Manchester', 'Birmingham', 'Bradford', 'Wolverhampton',
  'Sunderland', 'Sheffield', 'Leeds', 'Newcastle', 'Liverpool', 'Bristol', 'Glasgow', 'Wales', 'Scotland' ],
  countries: [ 'England', 'Scotland', 'Wales', 'Northern Ireland' ],
  employmentTypes: [ 'Full Time', 'Contractor', 'Intern', 'Part Time', 'Temporary', 'Volunteer', 'Permanent' ],
  payTypes: [ 'Salary', 'Monthly', 'Weekly', 'Hourly' ],
  // brands: [ 'Virgin Active', 'GLL', 'Nuffield Health', 'David Lloyds', 'CrossFit',
  // 'Serco', 'Fusion Lifestyle', 'Kingfisher', 'Places for People', 'Pure Gym', 'Fitness First',
  // 'The Gym Group', 'Center Parcs', 'Everyone Active' ]
};
