import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { TransactionService } from '../services/transaction.service';

@Injectable()
export class CreditGuard implements CanActivate {
  constructor(
    private transactionService: TransactionService,
    private router: Router
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    const isFreeTier = true;

    if (isFreeTier) {
      return true;
    } else {
      return this.transactionService.getCredits()
        .pipe(
          take(1),
          map(credit => {
            if (credit && credit.creditTotal > 0) {
              return true;
            } else {
              this.router.navigate(['/payment']);
              return false;
            }
          })
        );
    }
  }
}
