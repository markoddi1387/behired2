import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { AuthService } from '../services/auth.service';
import { NotificationModel } from '../models/notification.model';
import { NotificationService } from '../services/notification.service';
import { StorageService } from '../services/storage.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private notificationService: NotificationService,
    private router: Router,
    private storageService: StorageService
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    return this.authService.getUser()
      .pipe(
        take(1),
        map(user => {
          const uid = this.storageService.getUid();

          if (user && !user.emailVerified) {
            this.notificationService.updateNotification(new NotificationModel('verify', true));
          }

          if (user && uid && uid.length > 0) {
            return true;
          } else {
            this.authService.logout()
              .pipe(
                take(1)
              ).subscribe(() => this.router.navigate(['/signin']));
          }
        })
      );
  }
}
