import { Injectable, ErrorHandler } from '@angular/core';
import * as Sentry from '@sentry/browser';

@Injectable()
export class SentryErrorHandler implements ErrorHandler {
  handleError(error) {
    Sentry.captureException(error.originalError || error);
    throw error;
  }
}

Sentry.init({ dsn: 'https://3de381db31b146c88701952a2a6011f5@sentry.io/1361488' });
