# Steps to running and building:

- npm run build:ssr
- cd dist
- npm run prod:deploy

or

- To run JIT: npm run start

or

- To run AOT once build done, then do: npm run locally:prod
